<?php

use Illuminate\Database\Migrations\Migration;

class CreateOscarsTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

	Schema::create('categories', function($table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('points')->unsigned()->default(0);
			$table->integer('awarded')->unsigned()->default(0);
			$table->timestamps();
			$table->engine = 'InnoDB';
		});

	Schema::create('nominees', function($table)
		{
			$table->increments('id');
			$table->integer('category_id')->references('id')->on('categories');
			$table->string('name');
			$table->integer('winner')->unsigned()->default(0);
			$table->timestamps();
			$table->engine = 'InnoDB';
		});


	Schema::create('predictions', function($table)
		{
			$table->increments('id');
			$table->integer('user_group_id')->references('id')->on('user_groups');
			$table->integer('nominee_id')->references('id')->on('nominees');
			$table->integer('points')->unsigned()->default(0);
			$table->integer('locked')->unsigned()->default(0);
			$table->softDeletes();
			$table->timestamps();
			$table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('categories');
		Schema::dropIfExists('nominees');
		Schema::dropIfExists('predictions');
	}

}