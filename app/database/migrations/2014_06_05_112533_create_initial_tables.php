<?php

use Illuminate\Database\Migrations\Migration;

class CreateInitialTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

	Schema::create('countries', function($table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('description')->nullable();
			$table->string('flag_path')->nullable();
			$table->integer('group')->unsigned();
			$table->integer('status')->unsigned()->default(1);
			$table->integer('win')->unsigned()->default(0);
			$table->integer('lost')->unsigned()->default(0);
			$table->integer('draw')->unsigned()->default(0);
			$table->integer('gf')->unsigned()->default(0);
			$table->integer('ga')->unsigned()->default(0);
			$table->integer('pts')->unsigned()->default(0);

			$table->softDeletes();
			$table->timestamps();
			$table->engine = 'InnoDB';
		});

	
	Schema::create('users', function($table)
		{
			$table->increments('id');
			$table->string('username')->unique();
			$table->string('password');
			$table->string('remember_token')->nullable();
			$table->string('first_name');
			$table->string('last_name');
			$table->string('email');
			$table->integer('role')->unsigned();
			$table->integer('country')->references('id')->on('countries');
			$table->integer('payment_received')->unsigned()->default(0);

			$table->softDeletes();
			$table->timestamps();
			$table->engine = 'InnoDB';
		});

	Schema::create('groups', function($table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('creator_id')->references('id')->on('users');
			$table->string('description')->nullable();
			$table->string('notes')->nullable();
			$table->integer('public')->unsigned()->default(1);
			$table->decimal('contribution', 19, 4)->default(0);
			$table->string('password')->nullable();

			$table->softDeletes();
			$table->timestamps();
			$table->engine = 'InnoDB';
		});


	Schema::create('user_groups', function($table)
		{
			$table->increments('id');
			$table->integer('user_id')->references('id')->on('users');
			$table->integer('group_id')->references('id')->on('groups');
			$table->timestamp('joined')->nullable();
			$table->integer('total_points')->unsigned()->default(0);
			$table->integer('score_accurate')->unsigned()->default(0);
			$table->integer('winner_accurate')->unsigned()->default(0);
			$table->integer('prev_rank')->unsigned()->default(0);
			$table->integer('rank')->unsigned()->default(0);
			$table->integer('authorized')->unsigned()->default(1);

			$table->softDeletes();
			$table->timestamps();
			$table->engine = 'InnoDB';
		});

	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('countries');
		Schema::dropIfExists('users');
		Schema::dropIfExists('groups');;
		Schema::dropIfExists('user_groups');
	}

}