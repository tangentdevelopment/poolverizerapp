<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        Country::create(array('name' => 'Egypt','flag_path' => '/assets/images/flags/egypt-flag.jpg', 'group' => 1));
        Country::create(array('name' => 'Russia','flag_path' => '/assets/images/flags/russian-federation-flag.gif', 'group' => 1));
        Country::create(array('name' => 'Saudi Arabia','flag_path' => '/assets/images/flags/saudia-arabia-flag.jpg', 'group' => 1));
        Country::create(array('name' => 'Uruguay','flag_path' => '/assets/images/flags/uruguay-flag.gif', 'group' => 1));

        Country::create(array('name' => 'Iran','flag_path' => '/assets/images/flags/iran-flag.gif', 'group' => 2));
        Country::create(array('name' => 'Morocco','flag_path' => '/assets/images/flags/morocco-flag.jpg', 'group' => 2));
        Country::create(array('name' => 'Portugal','flag_path' => '/assets/images/flags/portugal-flag.gif', 'group' => 2));
        Country::create(array('name' => 'Spain','flag_path' => '/assets/images/flags/spain-flag.gif', 'group' => 2));

        Country::create(array('name' => 'Australia','flag_path' => '/assets/images/flags/australia-flag.gif', 'group' => 3));
        Country::create(array('name' => 'Denmark','flag_path' => '/assets/images/flags/denmark-flag.jpg', 'group' => 3));
        Country::create(array('name' => 'France','flag_path' => '/assets/images/flags/france-flag.gif', 'group' => 3));
        Country::create(array('name' => 'Peru','flag_path' => '/assets/images/flags/peru-flag.jpg', 'group' => 3));
        
        Country::create(array('name' => 'Argentina','flag_path' => '/assets/images/flags/argentina-flag.gif', 'group' => 4));
        Country::create(array('name' => 'Croatia','flag_path' => '/assets/images/flags/croatia-flag.gif', 'group' => 4));
        Country::create(array('name' => 'Iceland','flag_path' => '/assets/images/flags/iceland-flag.jpg', 'group' => 4));
        Country::create(array('name' => 'Nigeria','flag_path' => '/assets/images/flags/nigeria-flag.gif', 'group' => 4));

        Country::create(array('name' => 'Brazil','flag_path' => '/assets/images/flags/brazil-flag.gif', 'group' => 5));
        Country::create(array('name' => 'Costa Rica','flag_path' => '/assets/images/flags/costa-rica-flag.gif', 'group' => 5));
        Country::create(array('name' => 'Serbia','flag_path' => '/assets/images/flags/serbia-flag.jpg', 'group' => 5));
        Country::create(array('name' => 'Switzerland','flag_path' => '/assets/images/flags/switzerland-flag.gif', 'group' => 5));
        
        Country::create(array('name' => 'Germany','flag_path' => '/assets/images/flags/germany-flag.gif', 'group' => 6));
        Country::create(array('name' => 'South Korea','flag_path' => '/assets/images/flags/south-korea-flag.jpg', 'group' => 6));
        Country::create(array('name' => 'Mexico','flag_path' => '/assets/images/flags/mexico-flag.gif', 'group' => 6));
        Country::create(array('name' => 'Sweden','flag_path' => '/assets/images/flags/sweden-flag.jpg', 'group' => 6));

        Country::create(array('name' => 'Belgium','flag_path' => '/assets/images/flags/belgium-flag.gif', 'group' => 7));
        Country::create(array('name' => 'England','flag_path' => '/assets/images/flags/england-flag.jpg', 'group' => 7));
        Country::create(array('name' => 'Panama','flag_path' => '/assets/images/flags/panama-flag.jpg', 'group' => 7));
        Country::create(array('name' => 'Tunisia','flag_path' => '/assets/images/flags/tunisia-flag.jpg', 'group' => 7));

        Country::create(array('name' => 'Colombia','flag_path' => '/assets/images/flags/colombia-flag.gif', 'group' => 8));
        Country::create(array('name' => 'Japan','flag_path' => '/assets/images/flags/japan-flag.gif', 'group' => 8));
        Country::create(array('name' => 'Poland','flag_path' => '/assets/images/flags/poland-flag.jpg', 'group' => 8));
        Country::create(array('name' => 'Senegal','flag_path' => '/assets/images/flags/senegal-flag.jpg', 'group' => 8));

        Country::create(array('name' => 'TBD','flag_path' => '/assets/images/flags/tbd.jpg', 'group' => 9));
        
        Country::create(array('name' => 'United States','flag_path' => '/assets/images/flags/united-states.jpg', 'group' => 10));

        User::create(array(
        	'username' => '@poolverizeradmin',
            'password' => 'soccerpoolz2014',
            'first_name' => 'Admin',
            'last_name' => 'Admin',
            'email' => 'admin@poolverizer.com',
            'role' => 2,
            'country' => 3,
            'country_supported' => rand(1, 32),
        ));

        User::create(array(
        	'username' => 'usuariox',
            'password' => 'passdelx',
            'first_name' => 'Peter',
            'last_name' => 'Parker',
            'email' => 'peter@parker.com',
            'role' => 3,
            'country' => 3,
            'country_supported' => rand(1, 32),
        ));


        User::create(array(
        	'username' => 'usuario1',
            'password' => 'passdelx',
            'first_name' => 'Bruce',
            'last_name' => 'Wayne',
            'email' => 'bruce@wayne.com',
            'role' => 1,
            'country' => 3,
            'country_supported' => rand(1, 32),
        ));

        User::create(array(
        	'username' => 'usuario2',
            'password' => 'passdelx',
            'first_name' => 'Barry',
            'last_name' => 'Allen',
            'email' => 'barry@allen.com',
            'role' => 1,
            'country' => 3,
            'country_supported' => rand(1, 32),
        ));

        User::create(array(
        	'username' => 'usuario3',
            'password' => 'passdelx',
            'first_name' => 'Charles',
            'last_name' => 'Xavier',
            'email' => 'charles@xavier.com',
            'role' => 1,
            'country' => 3,
            'country_supported' => rand(1, 32),
        ));

        User::create(array(
        	'username' => 'usuario4',
            'password' => 'passdelx',
            'first_name' => 'Clark',
            'last_name' => 'Kent',
            'email' => 'clark@kent.com',
            'role' => 3,
            'country' => 3,
            'country_supported' => rand(1, 32),
        ));

        User::create(array(
        	'username' => 'usuario5',
            'password' => 'passdelx',
            'first_name' => 'Hal',
            'last_name' => 'Jordan',
            'email' => 'hal@jordan.com',
            'role' => 1,
            'country' => 3,
            'country_supported' => rand(1, 32),
        ));

        Game::create(array('country1' => 2, 'country2' => 3, 'game_date' => '2018-06-14 16:00', 'stadium' => 'Luzhniki Stadium', 'city' => 'Moscow', 'phase' => 1));
        Game::create(array('country1' => 1, 'country2' => 4, 'game_date' => '2018-06-15 13:00', 'stadium' => 'Ekaterinburg Arena', 'city' => 'Ekaterinburg', 'phase' => 1));
        Game::create(array('country1' => 6, 'country2' => 5, 'game_date' => '2018-06-15 16:00', 'stadium' => 'Saint Petersburg Stadium', 'city' => 'SaintPetersburg', 'phase' => 1));
        Game::create(array('country1' => 7, 'country2' => 8, 'game_date' => '2018-06-15 19:00', 'stadium' => 'Fisht Stadium', 'city' => 'Sochi', 'phase' => 1));
        Game::create(array('country1' => 11, 'country2' => 9, 'game_date' => '2018-06-16 11:00', 'stadium' => 'Kazan Arena', 'city' => 'Kazan', 'phase' => 1));
        Game::create(array('country1' => 13, 'country2' => 15, 'game_date' => '2018-06-16 14:00', 'stadium' => 'Spartak Stadium', 'city' => 'Moscow', 'phase' => 1));
        Game::create(array('country1' => 12, 'country2' => 10, 'game_date' => '2018-06-16 17:00', 'stadium' => 'Mordovia Arena', 'city' => 'Saransk', 'phase' => 1));
        Game::create(array('country1' => 14, 'country2' => 16, 'game_date' => '2018-06-16 20:00', 'stadium' => 'Kaliningrad Stadium', 'city' => 'Kaliningrad', 'phase' => 1));
        Game::create(array('country1' => 18, 'country2' => 19, 'game_date' => '2018-06-17 13:00', 'stadium' => 'Samara Arena', 'city' => 'Samara', 'phase' => 1));
        Game::create(array('country1' => 21, 'country2' => 23, 'game_date' => '2018-06-17 16:00', 'stadium' => 'Luzhniki Stadium', 'city' => 'Moscow', 'phase' => 1));
        Game::create(array('country1' => 17, 'country2' => 20, 'game_date' => '2018-06-17 19:00', 'stadium' => 'Rostov Arena', 'city' => 'Rostov-on-Don', 'phase' => 1));
        Game::create(array('country1' => 24, 'country2' => 22, 'game_date' => '2018-06-18 13:00', 'stadium' => 'Nizhny Novgorod Stadium', 'city' => 'NizhnyNovgorod', 'phase' => 1));
        Game::create(array('country1' => 25, 'country2' => 27, 'game_date' => '2018-06-18 16:00', 'stadium' => 'Fisht Stadium', 'city' => 'Sochi', 'phase' => 1));
        Game::create(array('country1' => 28, 'country2' => 26, 'game_date' => '2018-06-18 19:00', 'stadium' => 'Volgograd Arena', 'city' => 'Volgograd', 'phase' => 1));
        Game::create(array('country1' => 31, 'country2' => 32, 'game_date' => '2018-06-19 13:00', 'stadium' => 'Spartak Stadium', 'city' => 'Moscow', 'phase' => 1));
        Game::create(array('country1' => 29, 'country2' => 30, 'game_date' => '2018-06-19 17:00', 'stadium' => 'Mordovia Arena', 'city' => 'Saransk', 'phase' => 1));
        Game::create(array('country1' => 2, 'country2' => 1, 'game_date' => '2018-06-19 19:00', 'stadium' => 'Saint Petersburg Stadium', 'city' => 'SaintPetersburg', 'phase' => 1));
        Game::create(array('country1' => 7, 'country2' => 6, 'game_date' => '2018-06-20 13:00', 'stadium' => 'Luzhniki Stadium', 'city' => 'Moscow', 'phase' => 1));
        Game::create(array('country1' => 4, 'country2' => 3, 'game_date' => '2018-06-20 16:00', 'stadium' => 'Rostov Arena', 'city' => 'Rostov-on-Don', 'phase' => 1));
        Game::create(array('country1' => 5, 'country2' => 8, 'game_date' => '2018-06-20 19:00', 'stadium' => 'Kazan Arena', 'city' => 'Kazan', 'phase' => 1));
        Game::create(array('country1' => 11, 'country2' => 12, 'game_date' => '2018-06-21 13:00', 'stadium' => 'Ekaterinburg Arena', 'city' => 'Ekaterinburg', 'phase' => 1));
        Game::create(array('country1' => 10, 'country2' => 9, 'game_date' => '2018-06-21 16:00', 'stadium' => 'Samara Arena', 'city' => 'Samara', 'phase' => 1));
        Game::create(array('country1' => 13, 'country2' => 14, 'game_date' => '2018-06-21 19:00', 'stadium' => 'Nizhny Novgorod Stadium', 'city' => 'NizhnyNovgorod', 'phase' => 1));
        Game::create(array('country1' => 17, 'country2' => 18, 'game_date' => '2018-06-22 13:00', 'stadium' => 'Saint Petersburg Stadium', 'city' => 'SaintPetersburg', 'phase' => 1));
        Game::create(array('country1' => 16, 'country2' => 15, 'game_date' => '2018-06-22 16:00', 'stadium' => 'Volgograd Arena', 'city' => 'Volgograd', 'phase' => 1));
        Game::create(array('country1' => 19, 'country2' => 20, 'game_date' => '2018-06-22 19:00', 'stadium' => 'Kaliningrad Stadium', 'city' => 'Kaliningrad', 'phase' => 1));
        Game::create(array('country1' => 25, 'country2' => 28, 'game_date' => '2018-06-23 13:00', 'stadium' => 'Spartak Stadium', 'city' => 'Moscow', 'phase' => 1));
        Game::create(array('country1' => 21, 'country2' => 24, 'game_date' => '2018-06-23 16:00', 'stadium' => 'Fisht Stadium', 'city' => 'Sochi', 'phase' => 1));
        Game::create(array('country1' => 22, 'country2' => 23, 'game_date' => '2018-06-23 19:00', 'stadium' => 'Rostov Arena', 'city' => 'Rostov-on-Don', 'phase' => 1));
        Game::create(array('country1' => 26, 'country2' => 27, 'game_date' => '2018-06-24 13:00', 'stadium' => 'Nizhny Novgorod Stadium', 'city' => 'NizhnyNovgorod', 'phase' => 1));
        Game::create(array('country1' => 30, 'country2' => 32, 'game_date' => '2018-06-24 16:00', 'stadium' => 'Ekaterinburg Arena', 'city' => 'Ekaterinburg', 'phase' => 1));
        Game::create(array('country1' => 31, 'country2' => 29, 'game_date' => '2018-06-24 19:00', 'stadium' => 'Kazan Arena', 'city' => 'Kazan', 'phase' => 1));
        Game::create(array('country1' => 4, 'country2' => 2, 'game_date' => '2018-06-25 15:00', 'stadium' => 'Samara Arena', 'city' => 'Samara', 'phase' => 1));
        Game::create(array('country1' => 3, 'country2' => 1, 'game_date' => '2018-06-25 15:00', 'stadium' => 'Volgograd Arena', 'city' => 'Volgograd', 'phase' => 1));
        Game::create(array('country1' => 5, 'country2' => 7, 'game_date' => '2018-06-25 19:00', 'stadium' => 'Mordovia Arena', 'city' => 'Saransk', 'phase' => 1));
        Game::create(array('country1' => 8, 'country2' => 6, 'game_date' => '2018-06-25 19:00', 'stadium' => 'Kaliningrad Stadium', 'city' => 'Kaliningrad', 'phase' => 1));
        Game::create(array('country1' => 10, 'country2' => 11, 'game_date' => '2018-06-26 15:00', 'stadium' => 'Luzhniki Stadium', 'city' => 'Moscow', 'phase' => 1));
        Game::create(array('country1' => 9, 'country2' => 12, 'game_date' => '2018-06-26 15:00', 'stadium' => 'Fisht Stadium', 'city' => 'Sochi', 'phase' => 1));
        Game::create(array('country1' => 16, 'country2' => 13, 'game_date' => '2018-06-26 19:00', 'stadium' => 'Saint Petersburg Stadium', 'city' => 'SaintPetersburg', 'phase' => 1));
        Game::create(array('country1' => 15, 'country2' => 14, 'game_date' => '2018-06-26 19:00', 'stadium' => 'Rostov Arena', 'city' => 'Rostov-on-Don', 'phase' => 1));
        Game::create(array('country1' => 22, 'country2' => 21, 'game_date' => '2018-06-27 15:00', 'stadium' => 'Kazan Arena', 'city' => 'Kazan', 'phase' => 1));
        Game::create(array('country1' => 23, 'country2' => 24, 'game_date' => '2018-06-27 15:00', 'stadium' => 'Ekaterinburg Arena', 'city' => 'Ekaterinburg', 'phase' => 1));
        Game::create(array('country1' => 19, 'country2' => 17, 'game_date' => '2018-06-27 19:00', 'stadium' => 'Spartak Stadium', 'city' => 'Moscow', 'phase' => 1));
        Game::create(array('country1' => 20, 'country2' => 18, 'game_date' => '2018-06-27 19:00', 'stadium' => 'Nizhny Novgorod Stadium', 'city' => 'NizhnyNovgorod', 'phase' => 1));
        Game::create(array('country1' => 30, 'country2' => 31, 'game_date' => '2018-06-28 15:00', 'stadium' => 'Volgograd Arena', 'city' => 'Volgograd', 'phase' => 1));
        Game::create(array('country1' => 32, 'country2' => 29, 'game_date' => '2018-06-28 15:00', 'stadium' => 'Samara Arena', 'city' => 'Samara', 'phase' => 1));
        Game::create(array('country1' => 26, 'country2' => 25, 'game_date' => '2018-06-28 19:00', 'stadium' => 'Kaliningrad Stadium', 'city' => 'Kaliningrad', 'phase' => 1));
        Game::create(array('country1' => 27, 'country2' => 28, 'game_date' => '2018-06-28 19:00', 'stadium' => 'Mordovia Arena', 'city' => 'Saransk', 'phase' => 1));
        Game::create(array('country1' => 33, 'country2' => 33, 'game_date' => '2018-06-30 15:00', 'stadium' => 'Kazan Arena', 'city' => 'Kazan', 'phase' => 2));
        Game::create(array('country1' => 33, 'country2' => 33, 'game_date' => '2018-06-30 19:00', 'stadium' => 'Fisht Stadium', 'city' => 'Sochi', 'phase' => 2));
        Game::create(array('country1' => 33, 'country2' => 33, 'game_date' => '2018-07-01 15:00', 'stadium' => 'Luzhniki Stadium', 'city' => 'Moscow', 'phase' => 2));
        Game::create(array('country1' => 33, 'country2' => 33, 'game_date' => '2018-07-01 19:00', 'stadium' => 'Nizhny Novgorod Stadium', 'city' => 'NizhnyNovgorod', 'phase' => 2));
        Game::create(array('country1' => 33, 'country2' => 33, 'game_date' => '2018-07-02 15:00', 'stadium' => 'Samara Arena', 'city' => 'Samara', 'phase' => 2));
        Game::create(array('country1' => 33, 'country2' => 33, 'game_date' => '2018-07-02 19:00', 'stadium' => 'Rostov Arena', 'city' => 'Rostov-on-Don', 'phase' => 2));
        Game::create(array('country1' => 33, 'country2' => 33, 'game_date' => '2018-07-03 15:00', 'stadium' => 'Saint Petersburg Stadium', 'city' => 'SaintPetersburg', 'phase' => 2));
        Game::create(array('country1' => 33, 'country2' => 33, 'game_date' => '2018-07-03 19:00', 'stadium' => 'Spartak Stadium', 'city' => 'Moscow', 'phase' => 2));
        Game::create(array('country1' => 33, 'country2' => 33, 'game_date' => '2018-07-06 15:00', 'stadium' => 'Nizhny Novgorod Stadium', 'city' => 'NizhnyNovgorod', 'phase' => 3));
        Game::create(array('country1' => 33, 'country2' => 33, 'game_date' => '2018-07-06 19:00', 'stadium' => 'Kazan Arena', 'city' => 'Kazan', 'phase' => 3));
        Game::create(array('country1' => 33, 'country2' => 33, 'game_date' => '2018-07-07 15:00', 'stadium' => 'Samara Arena', 'city' => 'Samara', 'phase' => 3));
        Game::create(array('country1' => 33, 'country2' => 33, 'game_date' => '2018-07-07 19:00', 'stadium' => 'Fisht Stadium', 'city' => 'Sochi', 'phase' => 3));
        Game::create(array('country1' => 33, 'country2' => 33, 'game_date' => '2018-07-10 19:00', 'stadium' => 'Saint Petersburg Stadium', 'city' => 'SaintPetersburg', 'phase' => 4));
        Game::create(array('country1' => 33, 'country2' => 33, 'game_date' => '2018-07-11 19:00', 'stadium' => 'Luzhniki Stadium', 'city' => 'Moscow', 'phase' => 4));
        Game::create(array('country1' => 33, 'country2' => 33, 'game_date' => '2018-07-14 15:00', 'stadium' => 'Saint Petersburg Stadium', 'city' => 'SaintPetersburg', 'phase' => 5));
        Game::create(array('country1' => 33, 'country2' => 33, 'game_date' => '2018-07-15 16:00', 'stadium' => 'Luzhniki Stadium', 'city' => 'Moscow', 'phase' => 6));

		Point::create(array('phase' => 1, 'score' => 1, 'winner' => 1 ));
		Point::create(array('phase' => 2, 'score' => 2, 'winner' => 2 ));
		Point::create(array('phase' => 3, 'score' => 4, 'winner' => 4 ));
		Point::create(array('phase' => 4, 'score' => 8, 'winner' => 8 ));
		Point::create(array('phase' => 5, 'score' => 6, 'winner' => 6 ));
		Point::create(array('phase' => 6, 'score' => 10, 'winner' => 12 ));

		Group::create(array('name' => 'Quiniela Entre Amigos', 'creator_id' => 1, 'description' => 'NADBANK and friends', 'contribution' => 40));
        Group::create(array('name' => 'Karepi.com', 'creator_id' => 1, 'description' => 'Karepi.com Staff', 'contribution' => 20));
        Group::create(array('name' => 'Parlevel', 'creator_id' => 1, 'description' => 'Parlevel World Cup Pool', 'contribution' => 25));
        Group::create(array('name' => 'Google', 'creator_id' => 1, 'description' => 'World Cup Pool for Googlers', 'contribution' => 20));

		UserGroup::create(array('user_id'=>2, 'group_id'=>1, 'rank' => 0));
		UserGroup::create(array('user_id'=>3, 'group_id'=>1, 'rank' => 0));
		UserGroup::create(array('user_id'=>4, 'group_id'=>1, 'rank' => 0));
		UserGroup::create(array('user_id'=>5, 'group_id'=>1, 'rank' => 0));
		UserGroup::create(array('user_id'=>6, 'group_id'=>2, 'rank' => 0));
		UserGroup::create(array('user_id'=>7, 'group_id'=>2, 'rank' => 0));

		for($i=1; $i<7; $i++){
			for($p=1; $p<32; $p++){
				Score::create(array('user_group_id'=>$i, 'game_id' => $p, 'country1_goals' => rand(0,4), 'country2_goals' => rand(0,4) ));
			}
			
		}


	}
}