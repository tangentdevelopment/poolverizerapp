<?php

class Country extends BaseModel {

	protected $table = 'countries';
	// Don't forget to fill this array
	protected $fillable = array();

    public static $TEAMGROUPS = array(
        array('id' => 1, 'name' => 'A'),
        array('id' => 2, 'name' => 'B'),
        array('id' => 3, 'name' => 'C'),
        array('id' => 4, 'name' => 'D'),
        array('id' => 5, 'name' => 'E'),
        array('id' => 6, 'name' => 'F'),
        array('id' => 7, 'name' => 'G'),
        array('id' => 8, 'name' => 'H'),
    );

	public function scopeExceptPlaceholder($query)
	{
		return $query->where('id', '!=', 1);
	}

	public function users()
    {
        return $this->hasMany('User', 'country');
    }

    public function supporters()
    {
        return $this->hasMany('User', 'country_supported');
    }

    public function gamesLocal()
    {
        return $this->hasMany('Game', 'country1');
    }

    public function gamesVisit()
    {
        return $this->hasMany('Game', 'country2');
    }

    public function getTeamGroupAttribute($value)
    {
        return self::findTypeById(self::$TEAMGROUPS, $value);
    }

    public static function teamGroup($value)
    {
         $productType = self::findTypeById(self::$TEAMGROUPS, $value);
         return $productType['name'];
    }

}