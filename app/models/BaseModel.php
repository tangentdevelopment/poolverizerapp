<?php

class BaseModel extends Eloquent
{
	public static $rules = array();

    public static function boot()
    {
        parent::boot();

        // anytime an object is updated, log any changes that should be tracked
        self::updated(function($object)
        {
            if (isset($object->tracked) && is_array($object->tracked))
            {
                $oldModel = $object->getOriginal();

                foreach ($object->tracked as $attribute)
                {
                    if (array_key_exists($attribute, $oldModel) && $oldModel[$attribute] != $object->{$attribute})
                    {
                        try
                        {
                            $action = new ActionRecord();
                            $action->attribute_name = $attribute;
                            $action->old_value = $oldModel[$attribute];
                            $action->new_value = $object->{$attribute};
                            $action->username = Auth::user() ? Auth::user()->username : '@guest';
                            $object->actionRecords()->save($action);
                        }
                        catch (Exception $ex)
                        {
                            Log::error('Failed to record action record for attribute: ' . $attribute . ', with exception: ' . $ex->getMessage());
                        }
                    }
                }
            }
        });
    }

    // establish the morph many relationship
    public function actionRecords()
    {
        return $this->morphMany('ActionRecord', 'actionable');
    }

	public static function isValid($object, $rules = null)
    {
    	if (!$rules && !static::$rules)
    	{
			Log::error('No rules are set and Model::isValid() was called.');
    	}
    	else
    	{
    		$rules = $rules ? $rules : static::$rules;
	        $validator = Validator::make($object, $rules);
	        
	        if ($validator->passes())
	        {
	        	return true;
	        }
	        else
	        {
	        	Log::debug($validator->messages());
	        	return false;
	        }
	    }
    }

    public static function findTypeById($types, $id, $defaultIndex = 0)
    {
        $derivedType = $types[$defaultIndex];
        foreach ($types as $type)
        {
            if (isset($type['id']) && $type['id'] == $id)
            {
                $derivedType = $type;
                break;
            }
        }
        return $derivedType;
    }

    public static function findTypeByName($types, $name, $defaultIndex = 0)
    {
        $derivedType = $types[$defaultIndex];
        foreach ($types as $type)
        {
            if (isset($type['name']) && strtolower($type['name']) == strtolower($name))
            {
                $derivedType = $type;
                break;
            }
        }
        return $derivedType;
    }

    public function scopeExceptPlaceholder($query)
    {
        return $query->where('id', '!=', 1);
    }

    public function getCreatedAtAttribute($value)
    {
        return self::convertUtcToLocal($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return self::convertUtcToLocal($value);
    }

    public function getStartedAtAttribute($value)
    {
        return self::convertUtcToLocal($value);
    }

    public static function convertUtcToLocal($value)
    {
        if ($value)
        {
            $tz = Config::get('app.localTimezone', null);
            $utcDateTime =  Carbon::createFromFormat('Y-m-d H:i:s', $value);
            return Carbon::createFromTimestamp($utcDateTime->timestamp, $tz);
        }
        else
        {
            return null;
        }
    }

    public static function convertLocalToUtc($value){
        if ($value)
        {
            $tz = Config::get('app.localTimezone', null);
            $offsetHours = Carbon::now($tz)->offsetHours;
            return $value->addHours($offsetHours * -1);
        }
        else
        {
            return null;
        }
    }

    public static function getUtcOffset(){
        $tz = Config::get('app.localTimezone', null);

        if($tz){
            return $offsetHours = Carbon::now($tz)->offsetHours;                
        }else{
            return 0;
        }
    }
}