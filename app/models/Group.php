<?php

class Group extends \Eloquent {
    
    public static $rules = array(
        'name'  => 'required|min:3|max:20|unique:groups'
    );

	protected $table = 'groups';
	// Don't forget to fill this array
	protected $fillable = array();

    public function groupCreator()
    {
        return $this->belongsTo('User', 'creator_id');
    }

    public function scorecards()
    {   
        return $this->hasMany('Scorecard', 'group_id');
    }

    public function userGroups()
    {   
        return $this->hasMany('UserGroup', 'group_id');
    }

    public function numberOfMembers()
    {
        $members = UserGroup::where('group_id','=', $this->id)->get();

        return count($members);
    }


}