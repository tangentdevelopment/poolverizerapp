<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	public static $rules = array(
		'username'	=> 'required|min:3|max:20|alpha_num|unique:users',
		'first_name'=> 'required',
		'last_name' => 'required',
		'email' 	=> 'email | required',
	    'password'	=> 'required|min:8|max:40'
	);

	public function scopeExceptSuperuser($query)
    {
        return $query->where('id', '!=', 1);
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'remember_token';
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function myCountry()
    {
        return $this->belongsTo('Country', 'country');
    }

    public function supporting()
    {
        return $this->belongsTo('Country', 'country_supported');
    }

    public function groupsCreated()
    {
        return $this->hasMany('Group', 'creator_id');
    }

    public function scorecards()
    {
        return $this->hasMany('Scorecard', 'user_id');
    }

    public function getGroup()
    {
    	$userGroup = UserGroup::where('user_id', '=', $this->id)->first();

    	return $userGroup->group->name;
    }

    public function getGroupId()
    {
    	$userGroup = UserGroup::where('user_id', '=', $this->id)->first();

    	return $userGroup->group->id;
    }

    public function getUserPoints(){
    	$userGroup = UserGroup::where('user_id', '=', $this->id)->first();

    	return $userGroup->total_points;
    }

    public function getContribution()
    {
    	$userGroup = UserGroup::where('user_id', '=', $this->id)->first();

    	return $userGroup->group->contribution;
    }

    public function userGroups()
    {
        return $this->hasMany('UserGroup', 'user_id');
    }

    public function getTotalPoints()
    {
    	return $this->getGroup()->predictions->sum('points');
    }

}
