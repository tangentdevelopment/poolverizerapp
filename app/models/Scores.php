<?php

class Score extends \Eloquent {

	protected $table = 'scores';
	// Don't forget to fill this array
	protected $fillable = array('user_group_id', 'game_id', 'country1_goals', 'country2_goals');

    public function userGroup()
    {
        return $this->belongsTo('UserGroup', 'user_group_id');
    }

    public function game()
    {
        return $this->belongsTo('Game', 'game_id');
    }

}