<?php

class Category extends \Eloquent {

	protected $table = 'categories';
	// Don't forget to fill this array
	protected $fillable = array();


	public function predictions()
    {
        return $this->hasMany('Prediction', 'category_id');
    }

    public function nominees()
    {
        return $this->hasMany('Nominee', 'category_id');
    }

}