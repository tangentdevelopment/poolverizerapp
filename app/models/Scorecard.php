<?php

class Scorecard extends \Eloquent {

	protected $table = 'scorecards';
	// Don't forget to fill this array
	protected $fillable = array();

    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    public function group()
    {
        return $this->belongsTo('Group', 'group_id');
    }

    public function scores()
    {   
        return $this->hasMany('Score', 'scorecard_id');
    }


}