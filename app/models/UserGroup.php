<?php

class UserGroup extends \Eloquent {

	protected $table = 'user_groups';
	// Don't forget to fill this array
	protected $fillable = array();

    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    public function group()
    {
        return $this->belongsTo('Group', 'group_id');
    }

    public function scores()
    {
        return $this->hasMany('Score', 'user_group_id');
    }

    public function predictions()
    {
        return $this->hasMany('Prediction', 'user_group_id');
    }

    public function getTotalPoints()
    {
        return $this->predictions->sum('points');
    }

    public function getTotalCategoriesCorrect()
    {
        
        return Prediction::where('user_group_id', '=', $this->id)->where('points', '<>', 0)->count();
    }

}