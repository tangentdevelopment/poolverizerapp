<?php

class Nominee extends \Eloquent {

	protected $table = 'nominees';
	// Don't forget to fill this array
	protected $fillable = array('category_id', 'name', 'winner');

    public function category()
    {
        return $this->belongsTo('Category', 'category_id');
    }

    public function predictions()
    {
        return $this->hasMany('Prediction', 'category_id');
    }

    public function game()
    {
        return $this->belongsTo('Game', 'game_id');
    }

    public function picked($userGroupId){

        $userPick = Prediction::where('nominee_id', '=', $this->id)->where('user_group_id', '=', $userGroupId)->first();

        return $userPick? true : false;
    }
}