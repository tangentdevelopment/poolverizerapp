<?php

class Game extends \Eloquent {

	protected $table = 'games';
	// Don't forget to fill this array
	protected $fillable = array();

    const LOCAL_WON = 1;
    const VISITOR_WON =2;
    const DRAW = 3;

    const ROUND_OF_16 = 49;
    const QUARTER_FINALS = 57;
    const SEMI_FINALS = 61;
    const THIRD_PLACE = 63;
    const THE_FINAL = 64;

	public function scores()
    {
        return $this->hasMany('Score', 'game_id');
    }

    public function countryLocal()
    {
        return $this->belongsTo('Country', 'country1');
    }

        public function countryVisit()
    {
        return $this->belongsTo('Country', 'country2');
    }

    public static function getGamesPlayed()
    {
        return Game::where('played', '=', 1)->count();
    }

    public static function getTotalGames()
    {
        return Game::count();
    }
}