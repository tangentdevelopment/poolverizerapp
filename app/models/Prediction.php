<?php

class Prediction extends \Eloquent {

	protected $table = 'predictions';
	// Don't forget to fill this array
	protected $fillable = array('user_group_id', 'category_id', 'nominee_id');

    public function userGroup()
    {
        return $this->belongsTo('UserGroup', 'user_group_id');
    }

    public function category()
    {
        return $this->belongsTo('Category', 'category_id');
    }

    public function nominee()
    {
        return $this->belongsTo('Nominee', 'nominee_id');
    }

}