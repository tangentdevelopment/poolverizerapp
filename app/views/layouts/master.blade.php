<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<title>Poolverizer - Dashboard</title>
	
	<!-- Meta -->
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	
	<!-- 
	**********************************************************
	In development, use the LESS files and the less.js compiler
	instead of the minified CSS loaded by default.
	**********************************************************
	<link rel="stylesheet/less" href="/assets/less/admin/module.admin.page.index.less" />
	-->

		<!--[if lt IE 9]><link rel="stylesheet" href="/assets/components/library/bootstrap/css/bootstrap.min.css" /><![endif]-->
	<link rel="stylesheet" href="/css/admin/module.admin.page.index.min.css" />
	
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


<script src="/assets/components/library/jquery/jquery.min.js?v=v2.3.0"></script>
<script src="/assets/components/library/jquery/jquery-migrate.min.js?v=v2.3.0"></script>
<script src="/assets/components/library/modernizr/modernizr.js?v=v2.3.0"></script>
<script src="/assets/components/plugins/less-js/less.min.js?v=v2.3.0"></script>
<script src="/assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v2.3.0"></script>
<script src="/assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v2.3.0"></script>
<script src="/assets/components/library/jquery-ui/js/jquery-ui.min.js?v=v2.3.0"></script>
<script src="/assets/components/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js?v=v2.3.0"></script>	<script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51781518-1', 'poolverizer.com');
  ga('send', 'pageview');

</script>

</head>
<body class="document-body ">
	
		<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		
				<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
		
		<!-- Sidebar Menu -->
		<div id="menu" class="hidden-print">
		
			<!-- Brand -->
			<a href="/dashboard" class="appbrand"><span class="text-primary">POOL</span> <span>VERIZER</span></a>
		
			<!-- Scrollable menu wrapper with Maximum height -->
			<!-- <div class="slim-scroll" data-scroll-height="800px"> -->
			<div class="slim-scroll">
			
				<!-- Menu Toggle Button -->
				<button type="button" class="btn btn-navbar">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
				</button>
				<!-- // Menu Toggle Button END -->
				
				<!-- Sidebar Profile -->
				<span class="profile center">
					<a href="/dashboard"><img class="img-responsive" src="/assets/images/avatar-74x71.png" alt="Avatar" /></a>
				</span>
				<!-- // Sidebar Profile END -->
				
				<!-- Menu -->
				<ul>
				
					<li class="hasSubmenu">
	<a href="/dashboard" class="glyphicons dashboard"><i></i>Dashboard</a>
</li>
					<li class="hasSubmenu">
	<a href="/rankings" class="glyphicons podium"><i></i>Rankings</a>
</li>
					<li class="hasSubmenu">
	@if ($lang == 'en')                    
	<a href="http://www.paypal.com" target="_blank" class="glyphicons money"><i></i>Payment</a>
    @else
   	<a href="http://www.paypal.com" target="_blank" class="glyphicons money"><i></i>Pagos</a>
    @endif
</li>

<li class="hasSubmenu">
	@if ($lang == 'en')                    
	<a href="#menu_pages" data-toggle="collapse" class="glyphicons notes"><i></i><span>Pages</span><span class="fa fa-chevron-down"></span></a>
	<ul class="collapse" id="menu_pages">

		<li><a  target="_blank" href="https://www.fifa.com/worldcup/matches/" class="glyphicons calendar"><i></i>Match Schedule</a></li>
		<li><a  target="_blank" href="http://www.fifa.com/worldcup/groups/index.html" class="glyphicons podium"><i></i>Standings</a></li>
		<li><a  target="_blank"href="http://www.fifa.com/worldcup/teams/index.html" class="glyphicons globe"><i></i>Teams</a></li>
		<li><a  target="_blank"href="http://www.fifa.com/worldcup/groups/" class="glyphicons group"><i></i>Groups</a></li>
		<li><a  target="_blank"href="http://www.fifa.com/worldcup/matches/index.html" class="glyphicons soccer_ball"><i></i>Matches</a></li>
		<li><a href="/rules" class="glyphicons circle_question_mark"><i></i>Rules</a></li>
		<li><a href="/logout" class="glyphicons circle_remove"><i></i>Logout</a></li>
	
	</ul>
    @else
	<a href="#menu_pages" data-toggle="collapse" class="glyphicons notes"><i></i><span>Páginas</span><span class="fa fa-chevron-down"></span></a>
	<ul class="collapse" id="menu_pages">

		<li><a  target="_blank" href="https://www.fifa.com/worldcup/matches/" class="glyphicons calendar"><i></i>Calendario</a></li>
		<li><a  target="_blank" href="http://www.fifa.com/worldcup/groups/index.html" class="glyphicons podium"><i></i>Posiciones</a></li>
		<li><a  target="_blank"href="http://www.fifa.com/worldcup/teams/index.html" class="glyphicons globe"><i></i>Equipos</a></li>
		<li><a  target="_blank"href="http://www.fifa.com/worldcup/groups/" class="glyphicons group"><i></i>Grupos</a></li>
		<li><a  target="_blank"href="http://www.fifa.com/worldcup/matches/index.html" class="glyphicons soccer_ball"><i></i>Partidos</a></li>
		<li><a href="/rules" class="glyphicons circle_question_mark"><i></i>Reglas</a></li>
	
	</ul>
    @endif
</li>


					
				</ul>
				<div class="clearfix"></div>
				<!-- // Menu END -->
			
			
			</div>
			<!-- // Scrollable Menu wrapper with Maximum Height END -->
			
		</div>
		<!-- // Sidebar Menu END -->
				
		<!-- Content -->
		<div id="content">
		
		<!-- Top navbar -->
<div class="navbar main hidden-print">

	<!-- Menu Toggle Button -->
	<button type="button" class="btn btn-navbar pull-left visible-xs">
		<span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
	</button>
	<!-- // Menu Toggle Button END -->
	
	<!-- Full Top Style -->
	<ul class="topnav pull-left">

	</ul>
	
	<!-- Top Menu Right -->
	<ul class="topnav pull-right hidden-sm hidden-xs">	
				
		<!-- Language menu -->
		<li class="hidden-xs dropdown dd-1 dd-flags" id="lang_nav">
           	@if ($lang == 'en')
			<a href="#" data-toggle="dropdown"><img style="width:25px;" src="/assets/images/flags/united-states-flag.gif" alt="en" /></a>
            @else
			<a href="#" data-toggle="dropdown"><img style="width:25px;" src="/assets/images/flags/mexico-flag.gif" alt="sp" /></a>
            @endif
	    	<ul class="dropdown-menu pull-right">
               	@if ($lang == 'en')
	      		<li class="active"><a href="/dashboard/en" title="English"><img style="width:25px;" src="/assets/images/flags/united-states-flag.gif" alt="English"> English</a></li>
	      		<li><a href="/dashboard/sp" title="Español"><img style="width:25px;" src="/assets/images/flags/mexico-flag.gif" alt="Spanish"> Spanish</a></li>
                @else
   	      		<li class="active"><a href="/dashboard/sp" title="Español"><img style="width:25px;" src="/assets/images/flags/mexico-flag.gif" alt="Spanish"> Spanish</a></li>
	      		<li><a href="/dashboard/en" title="English"><img style="width:25px;" src="/assets/images/flags/united-states-flag.gif" alt="English"> English</a></li>
                @endif
	    	</ul>
		</li>
		<!-- // Language menu END -->
	
		<!-- Profile / Logout menu -->
		<li class="account dropdown dd-1">
						<a data-toggle="dropdown" href="/dashboard" class="glyphicons logout lock"><span class="hidden-md hidden-sm hidden-desktop-1">{{{ Auth::user()->username }}}</span><i></i></a>
			<ul class="dropdown-menu pull-right">
				<li><a href="/users/{{{ Auth::user()->id }}}/edit" class="glyphicons cogwheel">@if($lang == 'en') Your Account @else Tu Cuenta @endif<i></i></a></li>
				<li class="innerTB half">
					<span>
						<a class="btn btn-default btn-xs pull-right" href="{{{ action('HomeController@logout') }}}">Logout</a>
						<span class="clearfix"></span>
					</span>
				</li>
			</ul>
					</li>
		<!-- // Profile / Logout menu END -->
		
	</ul>
	<!-- // Top Menu Right END -->

	<div class="clearfix"></div>
	
</div>
<!-- Top navbar END -->



@yield('content')


<div class="innerLR innerB">

	<div class="row">
		<div class="col-md-12 tablet-column-reset">

			<div class="separator bottom"></div>
			<!-- // Stats Widgets END -->
			
			<!-- Widget -->
			
			<div class="row">
				<div class="col-md-6">
					
<!-- // Special Offers Widget END -->


					
				</div>
			</div>
						
		</div>

	</div>
	
</div>	
	
		
</div>
		<!-- // Content END -->
		
</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
				
		<div id="footer" class="hidden-print">
			
			<!--  Copyright Line -->
			<div class="copy">&copy; 2018 - <a href="http://www.poolverizer.com">Poolverizer</a> - All Rights Reserved. </div>
			<!--  End Copyright Line -->
	
		</div>
		<!-- // Footer END -->
		
	</div>
	<!-- // Main Container Fluid END -->
	

@yield('bottomscript')

	<!-- Global -->
	<script>
	var basePath = '',
		commonPath = '/assets/',
		rootPath = '../',
		DEV = false,
		componentsPath = '/assets/components/';
	
	var primaryColor = '#e5412d',
		dangerColor = '#b55151',
		infoColor = '#5cc7dd',
		successColor = '#609450',
		warningColor = '#ab7a4b',
		inverseColor = '#45484d';
	
	var themerPrimaryColor = primaryColor;
	</script>
	
	<script src="/assets/components/library/bootstrap/js/bootstrap.min.js?v=v2.3.0"></script>
<script src="/assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v2.3.0"></script>
<script src="/assets/components/plugins/breakpoints/breakpoints.js?v=v2.3.0"></script>
<script src="/assets/components/modules/admin/charts/flot/assets/lib/jquery.flot.js?v=v2.3.0"></script>
<script src="/assets/components/modules/admin/charts/flot/assets/lib/jquery.flot.resize.js?v=v2.3.0"></script>
<script src="/assets/components/modules/admin/charts/flot/assets/lib/plugins/jquery.flot.tooltip.min.js?v=v2.3.0"></script>
<script src="/assets/components/modules/admin/charts/flot/assets/custom/js/flotcharts.common.js?v=v2.3.0"></script>
<script src="/assets/components/modules/admin/charts/flot/assets/custom/js/flotchart-simple.init.js?v=v2.3.0"></script>
<script src="/assets/components/modules/admin/gallery/blueimp-gallery/assets/lib/js/blueimp-gallery.min.js?v=v2.3.0"></script>
<script src="/assets/components/modules/admin/gallery/blueimp-gallery/assets/lib/js/jquery.blueimp-gallery.min.js?v=v2.3.0"></script>
<script src="/assets/components/modules/admin/employees/assets/js/employees.js?v=v2.3.0"></script>
<script src="/assets/components/modules/admin/forms/elements/select2/assets/lib/js/select2.js?v=v2.3.0"></script>
<script src="/assets/components/modules/admin/forms/elements/select2/assets/custom/js/select2.init.js?v=v2.3.0"></script>
<script src="/assets/components/plugins/holder/holder.js?v=v2.3.0"></script>
<script src="/assets/components/modules/admin/widgets/widget-chat/assets/js/widget-chat.js?v=v2.3.0"></script>
<script src="/assets/components/plugins/slimscroll/jquery.slimscroll.js?v=v2.3.0"></script>
<script src="/assets/components/modules/admin/charts/easy-pie/assets/lib/js/jquery.easy-pie-chart.js?v=v2.3.0"></script>
<script src="/assets/components/modules/admin/charts/easy-pie/assets/custom/easy-pie.init.js?v=v2.3.0"></script>
<script src="/assets/components/modules/admin/twitter/assets/js/twitter.js?v=v2.3.0"></script>
<script src="/assets/components/plugins/jquery.event.move/js/jquery.event.move.js?v=v2.3.0"></script>
<script src="/assets/components/plugins/jquery.event.swipe/js/jquery.event.swipe.js?v=v2.3.0"></script>
<script src="/assets/components/core/js/megamenu.js?v=v2.3.0"></script>
<script src="/assets/components/core/js/core.init.js?v=v2.3.0"></script>	
</body>
</html>