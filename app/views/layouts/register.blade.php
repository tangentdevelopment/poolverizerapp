<!DOCTYPE html>
<html class="animations front fluid top-full menuh-top sticky-top">
	<head>
	 <title>Poolverizer - Login</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
  
  
  <link rel="stylesheet" href="/css/front/module.front.page.login.min.css" />
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">

</head>
<body class="login">

	@yield('content')

</body>
</html>