@extends('layouts.master')

@section('content')

<script type="text/javascript">
	function confirmDelete() {
		var accept = confirm("Are you sure you want to delete this user?");
		if (accept) {
			return true;
		} else {
			return false;
		}
	}
</script>

<ul class="breadcrumb">
	<li>You are here</li>
	<li><a href="/dashboard" class="glyphicons dashboard"><i></i> POOLVERIZER</a></li>
		@if($isEdit)
			@if($user->id == Auth::user()->id )
			@else
			<li class="divider"><i class="fa fa-caret-right"></i></li>
			<li><a href="/users">Users</a></li>
			@endif
		@endif
		<li class="divider"><i class="fa fa-caret-right"></i></li>
		@if(!$isEdit)
			<li>New User</li>
		@else
			@if($user->id == Auth::user()->id )
				<li>Your Profile</li>
			@else
				<li>Edit User</li>
			@endif
		@endif
	</ul>
				
	<div class="innerLR">
</div>



<div class="separator bottom"></div>

<div class="row innerLR">


	<div class="col-md-12">
		<div class="innerAll">
			@if(!$isEdit)
				{{ Form::model(new User(), array('action' => 'UserController@store', 'class' => 'box-form')) }}
			@else
				{{ Form::model($user, array('action' => array('UserController@update', $user->id), 'method' => 'PUT', 'class' => 'box-form')) }}
			@endif
			<h1 class="strong innerB half"><i class="fa fa-group text-primary icon-fixed-width"></i>
			@if(!$isEdit)
				New User
			@else
				@if($user->id == Auth::user()->id )
					Your Profile
				@else
					Edit User {{ $user->username }}
				@endif
			@endif
			</h1>
			<span class="pull-right">
				@if($isEdit)
				@if($user->id == Auth::user()->id )
					<a href="{{ action('HomeController@showDashboard') }}" class="btn btn-block btn-default" style="float:left;width:150px;margin-right:10px;" type="button">Cancel</a>
				@else
					<a href="{{ action('UserController@index') }}" class="btn btn-block btn-default" style="float:left;width:150px;margin-right:10px;" type="button">Cancel</a>
				@endif
				@else
					<a href="{{ action('UserController@index') }}" class="btn btn-block btn-default" style="float:left;width:150px;margin-right:10px;" type="button">Cancel</a>
				@endif
				@if(!$isEdit)
					{{ Form::submit('Save', array('class' => 'btn btn-block btn-info pull-right', 'style' => 'margin-top:0px;width:150px;')) }}
				@else
					{{ Form::submit('Update', array('class' => 'btn btn-block btn-info pull-right', 'style' => 'margin-top:0px;width:150px;')) }}
				@endif

			</span><br><br>
			<div class="col-md-6">
			
			<!-- Widget -->
			<div class="widget widget-heading-simple widget-body-white">				
				<div class="widget-body">
					@if(!$isEdit)
					{{ Form::text('username', null, array('class'=>'form-control', 'placeholder' => 'Username', 'id' => 'username')) }}
					@else
					{{ Form::text('username', null, array('class'=>'form-control', 'placeholder' => 'Username', 'id' => 'username', 'disabled' => 'disabled')) }}
					@endif
					<span class="help-block error">{{ $errors->has('username') ? $errors->first('username') : '' }}</span>
					{{ Form::text('first_name', null, array('class'=>'form-control', 'placeholder' => 'First Name', 'id' => 'first_name')) }}
					<span class="help-block error">{{ $errors->has('first_name') ? $errors->first('first_name') : '' }}</span>
					{{ Form::text('last_name', null, array('class'=>'form-control', 'placeholder' => 'Last Name', 'id' => 'last_name')) }}
					<span class="help-block error">{{ $errors->has('last_name') ? $errors->first('last_name') : '' }}</span>
					{{ Form::text('email', null, array('class'=>'form-control', 'placeholder' => 'Email', 'id' => 'email')) }}
					<span class="help-block error">{{ $errors->has('email') ? $errors->first('email') : '' }}</span>
					<input name="password" type="password" placeholder="@if($isEdit)New @endif Password" class="form-control" value="">
					<span class="help-block error">{{ $errors->has('password') ? $errors->first('password') : '' }}</span>
					@if(Auth::user()->id == 1)
					<h5>Group: </h5>
					<select name="group" class="form-control">
						@foreach($groups as $group)
						@if($isEdit)
						<option value="{{{ $group->id }}}" @if($group->name == $user->getGroup()) selected @endif>{{{ $group->name }}}</option>
						@else
						<option value="{{{ $group->id }}}">{{{ $group->name }}}</option>
						@endif
						@endforeach
					</select><br>
					@else
						@if($isEdit)
						<input type="hidden" name="group" value="{{{ $user->getGroupId() }}}">
						@else
						<input type="hidden" name="group" value="{{{ Auth::user()->getGroupId() }}}">
						@endif
					@endif
					@if($isEdit)
					<input type="hidden" name="role" value="{{{ $user->role }}}" />
					@endif
					@if(Auth::user()->id == 1)
					<h5>Role: </h5>
					<select name="role" class="form-control">
						@if($isEdit)
						@if($user->id == 1)
						<option value="2" selected>Super Admin</option>
						@else
						<option value="1" @if($user->role == 1) selected @endif >User</option>
						<option value="3" @if($user->role == 3) selected @endif>Group Admin</option>
						@endif
						@else
						<option value="1" selected>User</option>
						<option value="3">Group Admin</option>
						@endif
					</select><br>
					@endif
					<h5>Country: </h5>
					<select name="country" class="form-control">
						@foreach($userCountries as $country)
						<option value="{{{ $country->id }}}" @if($isEdit)@if($country->id == $user->country) selected @endif @endif >{{{ $country->name }}}</option>
						@endforeach
					</select><br>
							@if(Auth::user()->role !=1 )
							<input type="hidden" name="payment_received" value="0" />
							<input type="checkbox" name="payment_received" value="1" @if($isEdit)@if($user->payment_received == 1) checked @endif@endif>
								Payment Received
							@else
							<input type="hidden" name="payment_received" value="{{{ $user->payment_received }}}" />
							@endif
				
				</div>
			</div>
		</div>
		{{ Form::close() }}
			</div>
		</div>
			<!-- // Widget END -->
	</div>

@stop 