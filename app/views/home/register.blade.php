@extends('layouts.register')
@section('content')

<link href="/css/chosen.css" rel="stylesheet">
  <style type="text/css">
    .small-margin-top {
      margin-top: 10px;
    }
    .large-top-margin {
      margin-top: 300px;
    }
    .btn-cancel {
      background: #EEA2A2;
      border-color: #EEA2A2;
      color: #fff;
    }
  </style>
</head>
<body class="document-body login">
  
  <!-- Wrapper -->
<div id="login">

  <div class="wrapper signup">
    
      <h1><b>P<img style="margin-bottom:3px;" width="20px;" href="/" src="/img/o.png"><img width="20px;" style="margin-bottom:3px;" href="/" src="/img/o.png">LVERIZER</b><i></i></h1>
    
      <!-- Box -->
      <div class="widget widget-heading-simple">
        
        <div class="widget-head">
          <h3 class="heading">Create Account</h3>
          <div class="pull-right">
            Already a member?
            <a href="/login" class="btn btn-inverse btn-mini">Sign in</a>
          </div>

        </div>

        <div class="widget-body ">         
          <!-- Row -->
          <div class="row">
            {{ Form::model(new User(), array('action' => 'HomeController@doRegister', 'class' => 'box-form')) }}
            
            <!-- Column -->
            <div class="col-md-6 padding-none border-right">
              <div class="innerLR">
                <label class="strong">Username</label>
               {{ Form::text('username', null, array('class'=>'input-block-level form-control', 'placeholder' => 'Your Username', 'id' => 'username')) }}
               <span class="help-block error">{{ $errors->has('username') ? $errors->first('username') : '' }}</span>
               <label class="strong">Password</label>
               <input name="password" type="password" placeholder="Your Password" class="input-block-level form-control">
               <span class="help-block error">{{ $errors->has('password') ? $errors->first('password') : '' }}</span>
               <label class="strong">First Name</label>
               {{ Form::text('first_name', null, array('class'=>'input-block-level form-control', 'placeholder' => 'Your First Name', 'id' => 'first_name')) }}
               <span class="help-block error">{{ $errors->has('first_name') ? $errors->first('first_name') : '' }}</span>
               <label class="strong">Last Name</label>
               <span class="help-block error">{{ $errors->has('last_name') ? $errors->first('last_name') : '' }}</span>
               {{ Form::text('last_name', null, array('class'=>'input-block-level form-control', 'placeholder' => 'Your Last Name', 'id' => 'last_name')) }}
               <span class="help-block error">{{ $errors->has('last_name') ? $errors->first('last_name') : '' }}</span>
               <label class="strong">Email</label>
               {{ Form::text('email', null, array('class'=>'input-block-level form-control', 'placeholder' => 'Your Email', 'id' => 'email')) }}
               <span class="help-block error">{{ $errors->has('email') ? $errors->first('email') : '' }}</span>
              </div>
            </div>
            <!-- // Column END -->
            
            <!-- Column -->
            <div class="col-md-6 padding-none">
              <div class="innerLR">
               <div id="groupSelect">
                 <label class="strong small-margin-top">Group</label><br>
                 <select name="group" style="width:250px;" class="chosen-select" data-placeholder="Choose Your Group">
                  <option value="0"></option>
                 @foreach($groups as $group)
                  <option value="{{{ $group->id }}}">{{{ $group->name }}}</option>
                 @endforeach
                  </select>
                  <span class="text-center">
                    <p id="orSwitch" style="margin-top:10px;">Or</p>
                  </span>
                </div>
                  <a href="#" id="newGroupBtn" class="btn btn-icon-stacked btn-block btn-default small-margin-top" style="padding-left:70px;margin-bottom:10px;">Create New Group</a>
                  <a href="#" id="cancelBtn" style="display:none;margin-bottom:15px;padding-left:105px;" class="btn btn-icon-stacked btn-block btn-default small-margin-top" style="padding-left:70px;margin-bottom:10px;">Cancel</a>
                <div id="newGroupDiv" style="display:none;"> 
                  <label class="strong small-margin-top">Group Name</label>
                 {{ Form::text('name', null, array('class'=>'input-block-level form-control', 'placeholder' => 'Your Group Name', 'id' => 'group_name')) }}
                 <span class="help-block error">{{ $errors->has('name') ? $errors->first('name') : '' }}</span>
                 <label class="strong">Buy In (USD $)</label>
                 <input type="number" step="any" min="0" name="contribution" class="input-block-level form-control" placeholder="Amount each user will pay to play">
                 <input type="hidden" name="groupBool" id="groupBool" value="0">
                </div>
                <label class="strong small-margin-top">Your Country</label>
               <select name="country" class="form-control input-block-level">
               @foreach($userCountries as $country)
                <option value="{{{ $country->id }}}">{{{ $country->name }}}</option>
               @endforeach
                </select>
                {{ Form::submit('Join POOLVERIZER now', array('class' => 'btn btn-icon-stacked btn-block btn-danger glyphicons user_add small-margin-top')) }}
              </div>
            </div>
            <!-- // Column END -->
                {{ Form::close() }}
          </div>
          <!-- // Row END -->   
        </div>
        <!-- // Box END -->
        
      </div>
      
  </div>
  
</div>
<!-- // Wrapper END --> 


  <!-- Global -->
  <script>
  var basePath = '',
    commonPath = '../assets/',
    rootPath = '../',
    DEV = false,
    componentsPath = '../assets/components/';
  
  var primaryColor = '#e5412d',
    dangerColor = '#b55151',
    infoColor = '#5cc7dd',
    successColor = '#609450',
    warningColor = '#ab7a4b',
    inverseColor = '#45484d';
  
  var themerPrimaryColor = primaryColor;
  </script>
  <script src="../assets/components/library/jquery/jquery.min.js?v=v2.3.0"></script>
<script src="../assets/components/library/jquery/jquery-migrate.min.js?v=v2.3.0"></script>
<script src="../assets/components/library/modernizr/modernizr.js?v=v2.3.0"></script>
<script src="../assets/components/plugins/less-js/less.min.js?v=v2.3.0"></script>
<script src="../assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v2.3.0"></script>
<script src="../assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v2.3.0"></script> <script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>
  <script src="../assets/components/library/bootstrap/js/bootstrap.min.js?v=v2.3.0"></script>
<script src="../assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v2.3.0"></script>
<script src="../assets/components/plugins/breakpoints/breakpoints.js?v=v2.3.0"></script>
<script src="../assets/components/modules/admin/forms/elements/uniform/assets/lib/js/jquery.uniform.min.js?v=v2.3.0"></script>
<script src="../assets/components/modules/admin/forms/elements/uniform/assets/custom/js/uniform.init.js?v=v2.3.0"></script>
<script src="../assets/components/plugins/jquery.event.move/js/jquery.event.move.js?v=v2.3.0"></script>
<script src="../assets/components/plugins/jquery.event.swipe/js/jquery.event.swipe.js?v=v2.3.0"></script>
<script src="../assets/components/core/js/megamenu.js?v=v2.3.0"></script>
<script src="../assets/components/core/js/core.init.js?v=v2.3.0"></script>

<script src="/js/chosen.jquery.js" type="text/javascript"></script>
<script src="/js/chosen.proto.js" type="text/javascript"></script>  


<script type="text/javascript">
$( document ).ready(function() {

   // var queries = null;
  var query = document.URL.split('?')[1];
  if (query == 'err_grp=1') {
    console.log('hellos');
    $('#groupSelect').hide();
    $('#newGroupBtn').hide();
    $('#newGroupDiv').show();
    $('#cancelBtn').show();
    $('#groupBool').val(1);
  };
  console.log(query);


  $('#newGroupBtn').click(function(e) {
    e.preventDefault();
    $(this).hide();
    $('#groupSelect').hide();
    $('#newGroupDiv').show();
    $('#cancelBtn').show();
    $('#groupBool').val(1);
  });

  $('#cancelBtn').click(function(e) {
    e.preventDefault();
    $(this).hide();
    $('#newGroupDiv').hide();
    $('#groupBool').val(0);
    $('#groupSelect').show();
    $('#newGroupBtn').show();
  });

  $(".chosen-select").chosen();
});
</script>

  
@stop
