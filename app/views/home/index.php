<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Poolverizer - Your one resource for 89th Academy Awards Pools</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS  -->
    <link href="js/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="js/assets/fontawesome/css/font-awesome.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Exo+2:300,200,100|Montserrat:400,700' rel='stylesheet' type='text/css'>
    <!-- Plugin CSS -->
    <link href="js/assets/lightbox/css/lightbox.css" rel="stylesheet">
    <link href="js/assets/owl-carousel/owl.carousel.css" rel="stylesheet">
    <!-- Main CSS -->
    <link href="css/innovate.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body data-offset="50" data-spy="scroll" data-target="#nav">

<div id="slider">
    <div class="cycle-slideshow"  data-cycle-fx="fadeout" data-cycle-pause-on-hover=false data-cycle-overlay-template="<span class=title>{{title}}<span>">
        <div class="cycle-overlay"></div>
        <img src="img/backgroundfive.jpg" data-cycle-title="PICK FAVORITES" alt="Example Image">
        <img src="img/backgroundthree.jpg" data-cycle-title="WATCH THE SHOW" alt="Example Image">
        <img src="img/backgroundfour.jpg" data-cycle-title="WIN" alt="Example Image">
        <div id="progress"></div>
    </div>

</div>

<header id="header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <a href="#">
                    <h1 class="logo">P<img style="margin-bottom:5px;"src="img/o.png"><img style="margin-bottom:5px;margin-left:2px;margin-right:2px;" src="img/o.png">LVERIZER</h1>
                </a>
                <a class="nav-toggle"><i class="fa fa-bars"></i></a>
                <nav id="nav">
                    <ul class="nav">
                        <li><a href="#slider">HOME</a></li>
                        <li><a href="#features">HOW TO PLAY</a></li>
                        <li><a href="#about">TEAM</a></li>
                        <!-- <li><a href="#blog">NEWS</a></li> -->
                        <li><a href="#contact">CONTACT</a></li>
                        <li><a href="/login">LOGIN</a></li>
                        <li> <a target="_blank" href="https://twitter.com/PoolverizerNews"><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </nav>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</header>

<section id="bar" class="wrap pattern">
    <div class="container pad">
        <div class="row">
            <div class="col-xs-12">
                <h2>PICK. COMPETE. WIN.</h2>
                <p class="details">Poolverizer is the easiest way to manage your office's / family's / friends' Russia 2018 World Cup Pools <br>Are you ready to Compete? Register right now!</p>
                <a href="https://twitter.com/PoolverizerNews"><i class="fa fa-twitter"></i></a>
                <br>
                <br>
            </div>
        </div>
    </div>
</section>

<section id="features" class="wrap plain">
    <div class="container pad">
        <div class="row">
            <div class="col-xs-12">
                <h2>3 SIMPLE STEPS</h2>
                <p class="details">It doesn't take much to spice up Hollywood's biggest night!!</p>
                <br>
                <div class="row">
                    <a href="#" class="feature">
                        <div class="col-md-4" data-scrollreveal="enter from the bottom over 1.1s after 0.1s">
                            <div class="i-overlay">
                                <i class="fa fa-group"></i>
                            </div>
                            <h3>CREATE / JOIN A GROUP</h3>
                            <p>Log in and create a group to play with your office pals, your family or your friends. Invite them to play using your group's name and password. If you have already been invited, just create an account and look for the group's name and enter the password.</p>
                        </div>
                    </a>
                    <a href="#" class="feature">
                        <div class="col-md-4" data-scrollreveal="enter from the bottom over 1.1s after 0.3s">
                            <div class="i-overlay">
                                <i class="fa fa-pencil"></i>
                            </div>
                            <h3>PICK GAME SCORES</h3>
                            <p>Pick the final score of all 48 first round matches of the 2018 World Cup Russia. You can edit scores before the World Cup begins but after the tournament starts you will only be able to enter scores for those matches that haven't been played. For the Group Stage games you will get 1 point for each time you predict the match winner and 1 extra point if you predicted the exact final score.</p>
                        </div>
                    </a>
                    <a href="#" class="feature">
                        <div class="col-md-4" data-scrollreveal="enter from the bottom over 1.1s after 0.5s">
                            <div class="i-overlay">
                                <i class="fa fa-trophy"></i>
                            </div>
                            <h3>FOLLOW THE BROADCAST</h3>
                            <p>While watching the ceremony see how you rank by logging in to Poolverizer from your computer or any mobile device. In the main dashboard you will be able to see your position in the group as well as your friends' ranking.</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div> 
</section>

<section id="about" class="wrap light">
    <div class="container pad">
        <div class="row">
            <div class="col-xs-12">
                <h2>TEAM</h2>
                <p class="details">We want to help you have all the fun you can and get involved in every game of the 2018 Russia World Cup.</p>
                <div class="row">
                    <div class="col-md-3 col-md-offset-2 about-item" data-scrollreveal="enter from the left over 1.1s after 0.2s">
                        <div class="overlay">
                            <img src="https://media.licdn.com/dms/image/C4E03AQGnWHiyCe-eOQ/profile-displayphoto-shrink_200_200/0?e=1533168000&v=beta&t=8TL0Z69m2BObJDCveNE6W4fQg8SndNfIsYaXF-rrQ2U" alt="Rafael">
                        </div>
                        <br>
                        <h3>RAFA BARROSO</h3>
                        <p><i>Developer</i><br> Tech Junkie, Soccer Enthusiast, Star Wars Fan, Frustrated Musician, Movies / Sports / Travel Addict.</p>
                        <p class="social">
                            <a href="https://twitter.com/rbarroso78"><i class="fa fa-twitter"></i></a>  <a href="https://www.linkedin.com/in/rafael-barroso-84b56a20/" target="_blank"><i class="fa fa-linkedin"></i></a>
                        </p>
                    </div>
                     <div class="col-md-3 col-md-offset-2 about-item" data-scrollreveal="enter from the right over 1.1s after 0.2s">
                        <div class="overlay">
                            <img src="https://media.licdn.com/dms/image/C4D03AQH38_grzH7iOw/profile-displayphoto-shrink_800_800/0?e=1533168000&v=beta&t=wk-oQ2zHX5CwYcnmQFy2XgWCkVKbaHUp6dQXwMGS3zU" alt="Arturo Camacho">
                        </div>
                        <br>
                        <h3>ARTURO CAMACHO</h3>
                        <p><i>Developer </i><br>Flame/Smoke Demo Artist, Autodesk Certified Instructor, Tech Advisor. IT Sys Admin @ PIXOMONDO</p>
                        <p class="social">
                            <a href="https://twitter.com/Karepi"><i class="fa fa-twitter"></i></a>  <a href="https://www.linkedin.com/in/arturo-camacho-814b9413/" target="_blank"><i class="fa fa-linkedin"></i></a>
                        </p>
                    </div>
<!--                      <div class="col-md-3 col-md-offset-2" data-scrollreveal="enter from the right over 1.1s after 0.4s">
                        <div class="overlay">
                            <img src="https://media.licdn.com/dms/image/C4D03AQFl0U--MsWUnw/profile-displayphoto-shrink_800_800/0?e=1533168000&v=beta&t=VPHle5c0_3TMcl-0ze7KMGzHKWTUR3xvvAe0fYoqpbA" alt="David Diaz">
                        </div>
                        <br>
                        <h3>DAVID DIAZ</h3>
                        <p><i>Creator, Concept and Logistics</i><br> Economist. UAA 89-91, COLMEX, Warwick University.</p>
                        <p class="social">
                                 <a href="https://twitter.com/DavidDiazRomo"><i class="fa fa-twitter"></i></a>  <a href="https://www.linkedin.com/in/david-diaz-95722614/" target="_blank"><i class="fa fa-linkedin"></i></a>
                        </p>
                    </div> -->
                    <div class="col-md-3 col-md-offset-2" data-scrollreveal="enter from the right over 1.1s after 0.4s">
                        <div class="overlay">
                            <img src="https://media.licdn.com/dms/image/C5603AQGJyO0GLJ_elg/profile-displayphoto-shrink_800_800/0?e=1533168000&v=beta&t=AVt1ypilTYnz-9y_-s39WmJ-kEQU65iagu4CSIMz53o" alt="Corey Kepple">
                        </div>
                        <br>
                        <h3>COREY KEPPLE</h3>
                        <p><i>Developer</i><br> LAMP Full Stack Developer, Game of Thrones enthusiast, Red Sox, Patriots and Celtics hardcore fan.</p>
                        <p class="social">
                        <a href="https://www.linkedin.com/in/cckepple/"><i class="fa fa-linkedin"></i></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <section id="gallery" class="wrap dark">
    <div class="container pad">
        <div class="row">
            <div class="col-xs-12">
                <h2>GALLERY</h2>
                <p class="details">Hover actions have two variations. Pick the one that suits you project best.</p>

                <div id="items" class="row">
                    <div class="col-xs-3" data-scrollreveal="enter from the bottom over 1.1s">
                         <div class="overlay">
                            <img src="http://placehold.it/500x325" alt="Example Image">
                            <a href="http://placehold.it/500x325" data-lightbox="example-set">
                                <div class="overlay-board sm">
                                    <i class="fa fa-arrows-alt"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-3 visible-lg" data-scrollreveal="enter from the top over 1.1s after 0.2s">
                         <div class="overlay">
                            <img src="http://placehold.it/500x325" alt="Example Image">
                            <a href="http://placehold.it/500x325" data-lightbox="example-set">
                                <div class="overlay-board sm">
                                    <i class="fa fa-search"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-3 visible-lg" data-scrollreveal="enter from the bottom over 1.1s after 0.4s">
                         <div class="overlay">
                            <img src="http://placehold.it/500x325" alt="Example Image">
                            <a href="http://placehold.it/500x325" data-lightbox="example-set">
                                <div class="overlay-board sm">
                                    <i class="fa fa-thumbs-up"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-3" data-scrollreveal="enter from the top over 1.1s after 0.6s">
                         <div class="overlay">
                            <img src="http://placehold.it/500x325" alt="Example Image">
                            <a href="http://placehold.it/500x325" data-lightbox="example-set">
                                <div class="overlay-board sm">
                                    <i class="fa fa-comment"></i>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-3 visible-lg" data-scrollreveal="enter from the bottom over 1.1s">
                         <div class="overlay">
                            <img src="http://placehold.it/500x325" alt="Example Image">
                            <a href="http://placehold.it/500x325" data-lightbox="example-set">
                                <div class="overlay-board">
                                    <span class="btn btn-overlay icon-only"><i class="fa fa-arrows-alt"></i></span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-3" data-scrollreveal="enter from the top over 1.1s after 0.2s">
                         <div class="overlay">
                            <img src="http://placehold.it/500x325" alt="Example Image">
                            <a href="http://placehold.it/500x325" data-lightbox="example-set">
                                <div class="overlay-board">
                                    <span class="btn btn-overlay icon-only"><i class="fa fa-search"></i></span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-3" data-scrollreveal="enter from the bottom over 1.1s after 0.4s">
                         <div class="overlay">
                            <img src="http://placehold.it/500x325" alt="Example Image">
                            <a href="http://placehold.it/500x325" data-lightbox="example-set">
                                <div class="overlay-board">
                                    <span class="btn btn-overlay icon-only"><i class="fa fa-thumbs-up"></i></span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-3 visible-lg" data-scrollreveal="enter from the top over 1.1s after 0.6s">
                         <div class="overlay">
                            <img src="http://placehold.it/500x325" alt="Example Image">
                            <a href="http://placehold.it/500x325" data-lightbox="example-set">
                                <div class="overlay-board">
                                    <span class="btn btn-overlay icon-only"><i class="fa fa-comment"></i></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<section id="clients" class="wrap parallax"  data-0="background-position:0px 10%;" data-end="background-position:0px -200%;">
    <div class="static-overlay"></div>
    <div class="container pad">
        <div class="row">
            <div class="col-xs-12">
                <h2>CLIENTS</h2>
                <p class="details">Here are some of our satisfied clients from over our many years in business.</p>
                <div class="client-list">
                    <i data-toggle="tooltip" title="Dribbble" class="fa fa-dribbble"></i>
                    <i data-toggle="tooltip" title="Xing"  class="fa fa-xing"></i>
                    <i data-toggle="tooltip" title="Linux"  class="fa fa-linux"></i>
                    <i data-toggle="tooltip" title="Stack Overflow"  class="fa fa-stack-overflow"></i>
                    <i data-toggle="tooltip" title="Weibo"  class="fa fa-weibo"></i>
                    <i data-toggle="tooltip" title="MaxCDN"  class="fa fa-maxcdn"></i>
                    <i data-toggle="tooltip" title="ADN"  class="fa fa-adn"></i>
                </div>
            </div>
        </div>
    </div>
</section>
 -->
<!-- <section id="blog" class="wrap plain">
    <div class="container pad">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <h2>WORLD CUP 2018 NEWS</h2>
                <p class="details">You can easily place any Font Awesome Font in the hover action.</p>
                
                <div id="carousel" class="owl-carousel">
                    <div class="col-xs-12">
                        <div class="overlay">
                            <img src="http://placehold.it/500x325" alt="Example Image">
                            <a href="post.html">
                                <div class="overlay-board">
                                    <i class="fa fa-search"></i>
                                </div>
                            </a>
                        </div>
                        <h3>Join Our Creative Workshops</h3>
                        <p class="date">Feb. 21, 2013</p>
                    </div>
                    <div class="col-xs-12">
                        <div class="overlay">
                            <img src="http://placehold.it/500x325" alt="Example Image">
                            <a href="post.html">
                                <div class="overlay-board">
                                    <i class="fa fa-thumbs-o-up"></i>
                                </div>
                            </a>
                        </div>
                        <h3>Hack-a-thon Tips &amp; Tricks</h3>
                        <p class="date">Feb. 2, 2013</p>
                    </div>
                    <div class="col-xs-12">
                        <div class="overlay">
                            <img src="http://placehold.it/500x325" alt="Example Image">
                            <a href="post.html">
                                <div class="overlay-board">
                                    <i class="fa fa-lightbulb-o"></i>
                                </div>
                            </a>
                        </div>
                        <h3>The Keys to Oranization</h3>
                        <p class="date">Feb. 21, 2013</p>
                    </div>
                    <div class="col-xs-12">
                        <div class="overlay">
                            <img src="http://placehold.it/500x325" alt="Example Image">
                            <a href="post.html">
                                <div class="overlay-board">
                                    <i class="fa fa-thumbs-o-up"></i>
                                </div>
                            </a>
                        </div>
                        <h3>New Tech. Old Tech.</h3>
                        <p class="date">Feb. 2, 2013</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section> -->

<section id="contact" class="wrap pattern">
    <div class="container pad">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="logo" data-scrollreveal="enter from the bottom over 1.1s after 0.2s">P<img style="margin-bottom:9px;" src="img/oBig.png"><img src="img/oBig.png" style="margin-left:3px;margin-right:4px;margin-bottom:9px;">LVERIZER</h2>
                <p class="details">We'd love to hear from you. Contact us by email</p>
                
                <div class="contact-details">
                    <ul>
                        <li><a href="mailto:poolverizer@watchmakerlabs.com" target="_blank">poolverizer@watchmakerlabs.com</a></li>
                    </ul>
                </div>

    
            </div>
        </div>
    </div>
</section>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <p>
                    <a href="https://twitter.com/PoolverizerNews?lang=en"><i class="fa fa-twitter"></i></a>
                    &nbsp;| &nbsp;
                    <a href="mailto:poolverizer@watchmakerlabs.com">poolverizer@watchmakerlabs.com</a>
                </p>
            </div>
        </div>
    </div>
</footer>

<!-- Libs -->
<script src="http://maps.google.com/maps/api/js?sensor=false&amp;libraries=geometry&amp;v=3.7"></script>
<script src="js/assets/jquery-1.11.0.min.js"></script>
<script src="js/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="js/assets/scroll-reveal/js/scrollReveal.js"></script>
<script src="js/assets/owl-carousel/owl.carousel.min.js"></script>
<script src="js/assets/retina.min.js"></script>
<script src="js/assets/cycle2.min.js"></script>
<script src="js/assets/lightbox/js/lightbox-2.6.min.js"></script>
<script src="js/assets/placeholder.js"></script>
<script src="js/assets/maplace.min.js"></script>
<script src="js/assets/sticky.js"></script>
<script src="js/assets/skrollr.min.js"></script>
<script src="js/assets/validate.min.js"></script>

<!-- Theme Scripts -->
<script src="js/innovate.min.js"></script>

<!-- Your Custom JS -->
<script src="js/custom.js"></script>

</body>
</html>
