@extends('layouts.master')

@section('content')
<ul class="breadcrumb">
	@if ($lang == 'en')
	<li>You are here</li>
    @else
   	<li>Estás aquí</li>
    @endif
	<li><a href="/dashboard/{{{$lang}}}" class="glyphicons dashboard"><i></i> POOLVERIZER</a></li>
			<li class="divider"><i class="fa fa-caret-right"></i></li>
		<li>Dashboard</li>
	</ul>
				
	<div class="innerLR">
</div>

<div class="innerAll">
	<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	@if($lang == 'sp')
	<strong>Sabías que puedes invitar a más personas a Poolverizer aún cuando el Mundial ya ha comenzado? </strong>
	No podrán capturar marcadores para partidos anteriores o en curso, pero con suerte podrían alcanzar al resto
	@else
	<strong>Do you know you can invite more friends to join Poolverizer even after the World Cup has started? </strong>
	They won't be able to enter scores for past or ongoing games, but they can still try their luck and catch up!
	@endif
</div>
	@if ($lang == 'en')
	<h1 class="margin-none pull-left">{{{ Auth::user()->first_name }}}'s Dashboard</h1>
    @else
    <h1 class="margin-none pull-left">Dashboard de {{{ Auth::user()->first_name }}}</h1>
    @endif
	<div class="btn-group pull-right">
		<a target="_blank" href="https://twitter.com/PoolverizerNews" class="btn btn-info"><i class="fa fa-twitter"></i>@PoolverizerNews</a>
		@if(Auth::user()->id == 1 || Auth::user()->role == 3)
			@if ($lang == 'en')
			<a href="/users" class="btn btn-default"><i class="fa fa-fw  fa-group"></i>User Mgmt</a>
            @else
            <a href="/users" class="btn btn-default"><i class="fa fa-fw  fa-group"></i>Adm Usuarios</a>
            @endif
		@endif
        	@if ($lang == 'en')
			<a href="/game_results" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i>Game Results</a>
            @else
   			<a href="/game_results" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i>Resultados</a>
            @endif
		@if(Auth::user()->id != 1)
			@if ($lang == 'en')
			<a href="/user_scores/{{{ Auth::user()->id  }}}" class="btn btn-default"><i class="fa fa-fw fa-table"></i>Your Scorecard</a>
            @else
            <a href="/user_scores/{{{ Auth::user()->id  }}}" class="btn btn-default"><i class="fa fa-fw fa-table"></i>Tus Resultados</a>
            @endif
		@endif
	</div>
	<div class="clearfix"></div>
</div>
@if(Auth::user()->id != 1)
<!-- Stats Widgets -->
<div class="row border-top border-bottom row-merge margin-none">
	
	<div class="col-md-3 border-right innerAll">
		<div class=" padding-bottom-none-phone">
			<!-- Stats Widget -->
<a href="" class="widget-stats widget-stats-default widget-stats-4">
	@if ($lang == 'en')
	<span class="txt">Points</span>
    @else
   	<span class="txt">Puntos</span>
    @endif
	<span class="count">@if(Auth::user()->id != 1){{{ $userInfo->scores->sum('points') }}}@endif</span>
	<span class="glyphicons user"><i></i></span>
	<div class="clearfix"></div>
	<i class="icon-play-circle"></i>
</a>
<!-- // Stats Widget END -->


		</div>
	</div>
	<div class="col-md-3 border-right innerAll">
		<div class=" padding-bottom-none-phone">
			<!-- Stats Widget -->
<a href="" class="widget-stats widget-stats-gray widget-stats-4">
	<span class="txt">Ranking</span>
	<span class="count">@if(Auth::user()->id != 1){{{ $ranking }}}@endif<span>/{{{ count($allUserInfo) }}}</span></span>
	<span class="glyphicons cup"><i></i></span>
	<div class="clearfix"></div>
	<i class="icon-play-circle"></i>
</a>
<!-- // Stats Widget END -->


	</div>
</div>
	<div class="col-md-3 border-right innerAll">
		<div class=" padding-bottom-none-phone">
			<!-- Stats Widget -->
<a href="" class="widget-stats widget-stats-primary widget-stats-4">
	@if ($lang == 'en')
	<span class="txt">Progress - Group Stage</span></span>
    @else
   	<span class="txt">Progreso - Fase Grupos</span>
    @endif
	<span class="count">{{{  (Game::getTotalGames()!=0)? number_format(Game::getGamesPlayed()/Game::getTotalGames()*100, 1) : '0' }}}%</span>
	<span class="glyphicons refresh"><i></i></span>
	<div class="clearfix"></div>
	<i class="icon-play-circle"></i>
</a>
<!-- // Stats Widget END -->


		</div>
	</div>


	<div class="col-md-3">
		<div class=" padding-bottom-none-phone">
			<!-- Stats Widget -->
<a href="" class="widget-stats widget-stats-2">
	<span class="count">{{{ Game::getGamesPlayed() }}}</span>
	@if ($lang == 'en')    
	<span class="txt">Matches Played</span>
    @else
	<span class="txt">Juegos jugados</span>    
    @endif
</a>
<!-- // Stats Widget END -->

		</div>
	</div>
</div>
@endif
<!-- // Stats Widgets END -->

<div class="separator bottom"></div>
<div class="row border-top border-bottom innerLR">
	<div class="col-md-8">
		<div class="innerAll">
			@if ($lang == 'en')            
			<h2 class="strong innerB half"><i class="fa fa-bolt text-primary icon-fixed-width"></i> Rankings @if(Auth::user()->id != 1) for "{{{ $userInfo->user->getGroup() }}}" @endif</h2>
            @else
			<h2 class="strong innerB half"><i class="fa fa-bolt text-primary icon-fixed-width"></i> Rankings @if(Auth::user()->id != 1) de "{{{ $userInfo->user->getGroup() }}}" @endif</h2>          
            @endif
			<table class="table table-striped table-condensed margin-none">
				<thead>
					<tr>
           				@if ($lang == 'en')            
						<th class="center">Ranking</th>
						<th class="center">Username</th>
						<th class="center">Name</th>
						@if(Auth::user()->id == 1)
						<th class="center">Group</th>
						@endif
						<th class="center">Points</th>
						<th class="center">Winner Pick Pts</th>
						<th class="center">Exact Score Pts</th>
						<th class="center">Efficiency</th>
						<th class="center">Precision</th>
						<th class="center">Supporting</th>
						<th class="center">Paid</th>
                        @else
						<th class="center">Ranking</th>
						<th class="center">Usuario</th>
						<th class="center">Nombre</th>
						@if(Auth::user()->id == 1)
						<th class="center">Grupo</th>
						@endif
						<th class="center">Ptos</th>
						<th class="center">Ptos Ganador/Empate</th>
						<th class="center">Ptos Marcador</th>
						<th class="center">Eficiencia</th>
						<th class="center">Precisión</th>
						<th class="center">Favorito</th>
						<th class="center">Pagó</th>
                        @endif
					</thead>
					</tr>
				</thead>
				<tbody>
					<?php $j=0; 
					      $prevPoints = 0;?>
					@foreach($allUserInfo as $position)
					<tr>
						@if($position->scores->sum('points') != $prevPoints)
						<?php $j++; ?>
						@endif
						<td class="center">{{{ $j }}}</td>
						<td class="center" @if($position->user->id == Auth::user()->id) style="font-weight:bold;" @endif><a data-toggle="tooltip" data-original-title="From: {{{ $position->user->myCountry->name }}}" data-placement="right"  href="/user_scores/{{{ $position->user->id }}}">{{{ $position->user->username }}}</a></td>
						<td class="center">{{{ $position->user->first_name .' '.$position->user->last_name }}}</td>
						@if(Auth::user()->id == 1)
						<td class="center">{{{ $position->group->name }}}</td>
						@endif
						<td class="center" style="font-size:16px;"><b>{{{ $position->scores->sum('points') }}}</b></td>
						<td class="center">{{{ $position->scores->sum('winner_points') }}}</td>
						<td class="center">{{{ $position->scores->sum('score_points') }}}</td>
						<td class="center">@if(Game::getGamesPlayed()!=0) {{{ number_format($position->scores->sum('points')/Game::getGamesPlayed()/2*100, 1) }}} @else 0 @endif%</td>
						<td class="center">@if(Game::getGamesPlayed()!=0) {{{ number_format($position->scores->sum('score_points')/Game::getGamesPlayed()*100, 1) }}} @else 0 @endif%</td>
						<td class="center"><img data-toggle="tooltip" data-original-title="{{{ $position->user->supporting->name  }}}" data-placement="right" width="30px;" src="{{{ $position->user->supporting->flag_path }}}"></td>
                        @if ($lang == 'en')
						<td class="center">{{{ ($position->user->payment_received == 1)? 'Yes' : 'No'}}}</td>
                        @else
                        <td class="center">{{{ ($position->user->payment_received == 1)? 'Sí' : 'No'}}}</td>
                        @endif
					</tr>
					<?php $prevPoints = $position->scores->sum('points'); ?>
					@endforeach
				</tbody>
			</table>
			<div class="separator bottom"></div>
		</div>
	</div>
	<div class="col-md-4">
    	@if ($lang == 'en')
		<br><h2 class="margin-none separator bottom"><i class="fa fa-signal text-primary icon-money"></i> Prizes</h2>
        @else
   		<br><h2 class="margin-none separator bottom"><i class="fa fa-signal text-primary icon-money"></i> Premios</h2>
        @endif
<div class="widget widget-heading-simple widget-body-gray" data-toggle="collapse-widget">
	<div class="widget-body list">
		<ul>
            @if ($lang == 'en')
			<li>
				<span>1st Place</span>
				<span class="count">${{{ number_format(($totalMoney * 0.6),2) }}}</span>
			</li>
			<li>
				<span>2nd Place</span>
				<span class="count">${{{ number_format(($totalMoney * 0.25),2) }}}</span>
			</li>
			<li>
				<span>3rd Place</span>
				<span class="count">${{{ number_format(($totalMoney * 0.15),2) }}}</span>
			</li>
			<li>
				<span><b>Total Entries</b></span>
				<span class="count">${{{number_format($totalMoney, 2)}}}</span>
			</li>
			@if(Auth::user()->id != 1)
			<li>
				<span><b>Group's Admin:</b> {{{ $groupAdmin->username }}} / <a href="mailto:{{{ $groupAdmin->email}}}">{{{ $groupAdmin->email}}}</a></span>
			</li>
			<li>
				<span>Contribution per Person:  ${{{ number_format($groupAdmin->getContribution(),2) }}}</span>
			</li>
			@endif
            @else
			<li>
				<span>1er Lugar</span>
				<span class="count">${{{ number_format(($totalMoney * 0.6),2) }}}</span>
			</li>
			<li>
				<span>2do Lugar</span>
				<span class="count">${{{ number_format(($totalMoney * 0.25),2) }}}</span>
			</li>
			<li>
				<span>3er Lugar</span>
				<span class="count">${{{ number_format(($totalMoney * 0.15),2) }}}</span>
			</li>
			<li>
				<span><b>Dinero Total</b></span>
				<span class="count">${{{number_format($totalMoney, 2)}}}</span>
			</li>
			@if(Auth::user()->id != 1)
			<li>
				<span><b>Admin. Grupo:</b> {{{ $groupAdmin->username }}} / <a href="mailto:{{{ $groupAdmin->email}}}">{{{ $groupAdmin->email}}}</a></span>
			</li>
			<li>
				<span>Contribución por Persona:  ${{{ number_format($groupAdmin->getContribution(),2) }}}</span>
			</li>
			@endif
            @endif
		</ul>
	</div>
<br><br>
	@if ($lang == 'en')
	<h2 class="margin-none separator bottom"><i class="fa fa-signal text-primary  icon-graph-up-1"></i> Points</h2>
    @else
   	<h2 class="margin-none separator bottom"><i class="fa fa-signal text-primary  icon-graph-up-1"></i> Puntos</h2>
    @endif
		<div class="widget-body list">
<table class="table table-striped table-condensed margin-none">
	<thead>
    @if ($lang == 'en')
	<tr>
		<th class="center">Matches</th>
		<th class="center">Winner Pick</th>
		<th class="center">Exact Score</th>
		<th class="center">Weighted Average(*)</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td class="center">Group Stage</td>
		<td class="center">1</td>
		<td class="center">1</td>
		<td class="center">42%</td>
	</tr>
		<tr>
		<td class="center">Round of 16</td>
		<td class="center">2</td>
		<td class="center">2</td>
		<td class="center">14%</td>
	</tr>
		<tr>
		<td class="center">Quarter Final</td>
		<td class="center">4</td>
		<td class="center">4</td>
		<td class="center">14%</td>
	</tr>
		<tr>
		<td class="center">Semi Final</td>
		<td class="center">8</td>
		<td class="center">8</td>
		<td class="center">14%</td>
	</tr>
	<tr>
		<td class="center">3rd and 4th</td>
		<td class="center">6</td>
		<td class="center">6</td>
		<td class="center">5%</td>
	</tr>
	<tr>
		<td class="center">Final</td>
		<td class="center">10</td>
		<td class="center">12</td>
		<td class="center">10%</td>
	</tr>
    @else
	<tr>
		<th class="center">Partidos</th>
		<th class="center">Ganador/Empate</th>
		<th class="center">Marcador</th>
		<th class="center">Media Ponderada(*)</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td class="center">Etapa de Grupos</td>
		<td class="center">1</td>
		<td class="center">1</td>
		<td class="center">42%</td>
	</tr>
		<tr>
		<td class="center">16avos de Final</td>
		<td class="center">2</td>
		<td class="center">2</td>
		<td class="center">14%</td>
	</tr>
		<tr>
		<td class="center">4tos de Final</td>
		<td class="center">4</td>
		<td class="center">4</td>
		<td class="center">14%</td>
	</tr>
		<tr>
		<td class="center">Semifinal</td>
		<td class="center">8</td>
		<td class="center">8</td>
		<td class="center">14%</td>
	</tr>
	<tr>
		<td class="center">3ro y 4to</td>
		<td class="center">6</td>
		<td class="center">6</td>
		<td class="center">5%</td>
	</tr>
	<tr>
		<td class="center">Final</td>
		<td class="center">10</td>
		<td class="center">12</td>
		<td class="center">10%</td>
	</tr>    
    @endif
</tbody>
</table>
@if ($lang == 'en')
<span style="font-size:11px;">(*) What each stage weights on the overall tournament</span>
@else
<span style="font-size:11px;">(*) El peso de cada etapa en el torneo</span>
@endif
	</div>
</div>
</div>
</div>
@stop 