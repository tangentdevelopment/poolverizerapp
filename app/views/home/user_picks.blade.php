@extends('layouts.master')

@section('content')
				
<ul class="breadcrumb">
@if ($lang == 'en')
	<li>You are here</li>
	<li><a href="/dashboard/{{{ $lang }}}" class="glyphicons dashboard"><i></i> POOLVERIZER</a></li>
			<li class="divider"><i class="fa fa-caret-right"></i></li>
		<li>Your Picks</li>
	</ul>
@else
	<li>Estás aquí</li>
	<li><a href="/dashboard/{{{ $lang }}}" class="glyphicons dashboard"><i></i> POOLVERIZER</a></li>
			<li class="divider"><i class="fa fa-caret-right"></i></li>
		<li>Tus Predicciones</li>
	</ul>
@endif				
	<div class="innerLR">
	<div class="innerB">
@if ($lang == 'en')    
		<h1 class="pull-left margin-none ">@if(Auth::user()->id == $thisUser->id) Your @else {{{ $thisUser->first_name }}} {{{ $thisUser->last_name }}}'s @endif Picks</h1><br> <span style="margin-left:20px;">| From: <img  data-toggle="tooltip" data-original-title="{{{ $thisUser->myCountry->name  }}}" data-placement="right" width="30px;" src="{{{ $thisUser->myCountry->flag_path }}}"></span>
				<a href="/cats_awarded" class="btn btn-default pull-right"><i class="fa fa-fw fa-trophy"></i>Awarded Categories</a>
		<div class="clearfix">		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<a href="" class="widget-stats widget-stats-gray widget-stats-2">
				<span class="count">{{{ $userGroup->total_points }}}</span>
				<span class="txt">Total Pts</span>
			</a>
		</div>
				<div class="col-md-6">
			<a href="" class="widget-stats widget-stats-gray widget-stats-2">
				<span class="count">{{{ $categoriesAwarded != 0 ? number_format(($userGroup->getTotalCategoriesCorrect() / $categoriesAwarded * 100),1) : 0}}}%</span>
				<span class="txt">Precision</span>
			</a>
		</div>
		<div class="col-md-12">
			<div class="separator"></div>
			<!-- Widget -->
			<div class="widget widget-body-white">
				<div class="widget-body">
						<div class="clearfix"></div>
					<!-- // Total bookings & sort by options END -->
					
					<!-- Table -->
@else
		<h1 class="pull-left margin-none ">@if(Auth::user()->id == $thisUser->id) Tus Predicciones @else Predicciones de {{{ $thisUser->first_name }}} {{{ $thisUser->last_name }}}@endif</h1><br> <span style="margin-left:20px;">| De: <img  data-toggle="tooltip" data-original-title="{{{ $thisUser->myCountry->name  }}}" data-placement="right" width="30px;" src="{{{ $thisUser->myCountry->flag_path }}}"></span>
				<a href="/cats_awarded" class="btn btn-default pull-right"><i class="fa fa-fw fa-trophy"></i>Categorías Premiadas</a>
		<div class="clearfix">		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<a href="" class="widget-stats widget-stats-gray widget-stats-2">
				<span class="count">{{{ $userGroup->total_points }}}</span>
				<span class="txt">Total de Puntos</span>
			</a>
		</div>
				<div class="col-md-6">
			<a href="" class="widget-stats widget-stats-gray widget-stats-2">
				<span class="count">{{{ $categoriesAwarded != 0 ? number_format(($userGroup->getTotalCategoriesCorrect() / $categoriesAwarded * 100),1) : 0 }}}%</span>
				<span class="txt">Precisión</span>
			</a>
		</div>
		<div class="col-md-12">
			<div class="separator"></div>
			<!-- Widget -->
			<div class="widget widget-body-white">
				<div class="widget-body">
						<div class="clearfix"></div>
					<!-- // Total bookings & sort by options END -->
					
					<!-- Table -->
@endif
					
					@foreach($allCategories as $category)
					<table style="width:80%", class="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs">
						<thead>
							<tr><th><h3>@if ($lang == 'en'){{{ $category->name }}}@else {{{ $category->name_sp }}} @endif<span style="font-size:14px;">- {{{ $category->points }}} Pts</span></h3></th>
							<th>Pick</th>
							<th>Winner</th>
							</tr>
						</thead>
						<tbody>
							@foreach($category->nominees as $nominee)
							<tr><td width="70%">@if($nominee->picked($userGroup->id))<b>@endif{{{ $nominee->name }}}@if($nominee->picked($userGroup->id))</b>@endif</td>
							<td witth="15%" @if($nominee->picked($userGroup->id)) style="background-color:rgb(221, 197, 67);" @endif></td>
							<td width="15%" @if($nominee->winner == 1) style="background-color:green;" @endif></td>
							</tr>
							@endforeach
						</tbody>
					</table>
					@endforeach
					<!-- // Table END -->													

				</div>  
				<!-- //end widget body -->
			</div>  
			<!-- //End Widget -->
		</div>
	</div>
	<!-- End Row	 -->						
</div>
<!-- End inner -->




	
	
	<!-- // Main Container Fluid END -->

	@stop