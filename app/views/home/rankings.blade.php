@extends('layouts.master')

@section('content')

<ul class="breadcrumb">
	@if ($lang == 'en')
	<li>You are here</li>
    @else
   	<li>Estás aquí</li>
    @endif
	<li><a href="/dashboard/{{{$lang}}}" class="glyphicons podium"><i></i> POOLVERIZER</a></li>
			<li class="divider"><i class="fa fa-caret-right"></i></li>
		<li>Rankings</li>
	</ul>
				
	<div class="innerLR">
</div>

<div class="innerAll">
@if ($lang == 'en')
	<h1 class="margin-none pull-left">{{{ Auth::user()->first_name }}}'s Dashboard &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i></h1>
	<div class="btn-group pull-right">
		@if(Auth::user()->id == 1)
			<a href="/results_entry" class="btn btn-default"><i class="fa fa-fw  fa-table"></i>Results Entry</a>
		@endif
		@if(Auth::user()->id == 1 || Auth::user()->role == 3)
			
			<a href="/users" class="btn btn-default"><i class="fa fa-fw  fa-group"></i>User Mgmt</a>
		@endif
			<a href="/game_results" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i>Game Results</a>
		@if(Auth::user()->id != 1)
			<a href="/user_scores/{{{ Auth::user()->id  }}}" class="btn btn-default"><i class="fa fa-fw fa-table"></i>Your Scorecard</a>
		@endif
	</div>
	<div class="clearfix"></div>
@else
	<h1 class="margin-none pull-left">Dashboard de {{{ Auth::user()->first_name }}} &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i></h1>
	<div class="btn-group pull-right">
		@if(Auth::user()->id == 1)
			<a href="/results_entry" class="btn btn-default"><i class="fa fa-fw  fa-table"></i>Introducir Resultados</a>
		@endif
		@if(Auth::user()->id == 1 || Auth::user()->role == 3)
			
			<a href="/users" class="btn btn-default"><i class="fa fa-fw  fa-group"></i>Adm Usuarios</a>
		@endif
			<a href="/game_results" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i>Resultados</a>
		@if(Auth::user()->id != 1)
			<a href="/user_scores/{{{ Auth::user()->id  }}}" class="btn btn-default"><i class="fa fa-fw fa-table"></i>Tus Resultados</a>
		@endif
	</div>
	<div class="clearfix"></div>
@endif
</div>

<!-- Stats Widgets -->
<div class="row border-top border-bottom row-merge margin-none">
	
	<div class="col-md-3 border-right innerAll">
		<div class=" padding-bottom-none-phone">
			<!-- Stats Widget -->
<a href="" class="widget-stats widget-stats-default widget-stats-4">
	@if ($lang == 'en')
	<span class="txt">Points</span>
    @else
   	<span class="txt">Puntos</span>
    @endif
	<span class="count">{{{ $userInfo->total_points }}}</span>
	<span class="glyphicons cup"><i></i></span>
	<div class="clearfix"></div>
	<i class="icon-play-circle"></i>
</a>
<!-- // Stats Widget END -->


		</div>
	</div>
	<div class="col-md-3 border-right innerAll">
		<div class=" padding-bottom-none-phone">
			<!-- Stats Widget -->
<a href="" class="widget-stats widget-stats-gray widget-stats-4">
	<span class="txt">Ranking</span>
	<span class="count">@if(Auth::user()->id != 1){{{ $ranking }}}@endif<span>/{{{ count($allUserInfo) }}}</span></span>
	<span class="glyphicons user"><i></i></span>
	<div class="clearfix"></div>
	<i class="icon-play-circle"></i>
</a>
<!-- // Stats Widget END -->


	</div>
</div>
	<div class="col-md-3 border-right innerAll">
		<div class=" padding-bottom-none-phone">
			<!-- Stats Widget -->
<a href="" class="widget-stats widget-stats-primary widget-stats-4">
	@if ($lang == 'en')
	<span class="txt">Progress - 3rd Place & Final</span>
    @else
	<span class="txt">Progreso - 3er Lugar y Final</span>    
    @endif
	<span class="count">{{{ (Game::getGamesPlayed() !=0)? number_format(Game::getGamesPlayed()/Game::getTotalGames()*100, 1) : '0' }}}%</span>
	<span class="glyphicons refresh"><i></i></span>
	<div class="clearfix"></div>
	<i class="icon-play-circle"></i>
</a>
<!-- // Stats Widget END -->


		</div>
	</div>


	<div class="col-md-3">
		<div class=" padding-bottom-none-phone">
			<!-- Stats Widget -->
<a href="" class="widget-stats widget-stats-2">
	<span class="count">{{{ Game::getGamesPlayed() }}}</span>
    @if ($lang == 'en')
	<span class="txt">Matches Played</span>
    @else
   	<span class="txt">Partidos Jugados</span>
    @endif
</a>
<!-- // Stats Widget END -->

		</div>
	</div>
</div>
<!-- // Stats Widgets END -->


<div class="separator bottom"></div>
<div class="row border-top border-bottom innerLR">
    <div class="col-md-8">
        <div class="innerAll">
            @if ($lang == 'en')
            <h2 class="strong innerB half"><i class="fa fa-bolt text-primary icon-fixed-width"></i>Global Rankings for "{{{ $userInfo->user->first_name .' '.$userInfo->user->last_name }}}"</h2>
                <table class="table table-striped table-condensed margin-none">
                    <thead>
                        <tr>
                            <th class="center">Ranking</th>
                            <th class="center">Username</th>
                            <th class="center">Points</th>
                            <th class="center">Winner Pick Pts</th>
                            <th class="center">Exact Score Pts</th>
                            <th class="center">Efficiency</th>
                            <th class="center">Precision</th>
                            <th class="center">Supporting</th>
                            <th class="center">Country</th>
                        </thead>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $j=0; 
                          $prevPoints = 0;?>
                        @foreach($allUserInfo as $position)
                        @if($position->scores->sum('points') != $prevPoints)
                        <?php $j++; ?>
                        @endif
                        @if (Auth::user()->id == $position->user->id)
                        <tr>
                            <td class="center"><strong>{{{ $j }}}</strong></td>
                            <td class="center"><strong><a data-toggle="tooltip" data-original-title="From: {{{ $position->user->myCountry->name }}}" data-placement="right"  href="/user_scores/{{{ $position->user->id }}}">{{{ $position->user->username }}}</a></strong></td>
                            <td class="center"><strong>{{{ $position->total_points }}}</strong></td>
                            <td class="center"><strong>{{{ $position->scores->sum('winner_points') }}}</strong></td>
                            <td class="center"><strong>{{{ $position->scores->sum('score_points') }}}</strong></td>
                            <td class="center">{{{ (Game::getGamesPlayed()!=0)? $position->scores->sum('points')/226*100: '0' }}}%</td>
                            <td class="center">{{{ (Game::getGamesPlayed()!=0)? $position->scores->sum('score_points')/Game::getGamesPlayed()*100 : '0' }}}%</td>
                            <td class="center"><strong><img data-toggle="tooltip" data-original-title="{{{ $position->user->supporting->name  }}}" data-placement="right" width="30px;" src="{{{ $position->user->supporting->flag_path }}}"></strong></td>
                            <td class="center"><strong>{{{ $position->user->myCountry->name }}}</strong></td>
                        </tr>
                        @else
                        <tr>
                            <td class="center">{{{ $j }}}</td>
                            <td class="center"><a data-toggle="tooltip" data-original-title="From: {{{ $position->user->myCountry->name }}}" data-placement="right"  href="/user_scores/{{{ $position->user->id }}}">{{{ $position->user->username }}}</a></td>
                            <td class="center">{{{ $position->total_points }}}</td>
                            <td class="center">{{{ $position->scores->sum('winner_points') }}}</td>
                            <td class="center">{{{ $position->scores->sum('score_points') }}}</td>
                            <td class="center">{{{ (Game::getGamesPlayed()!=0)? number_format($position->scores->sum('points')/226*100, 1) : '0' }}}%</td>
                            <td class="center">{{{ (Game::getGamesPlayed()!=0)? number_format($position->scores->sum('score_points')/Game::getGamesPlayed()*100, 1) : '0' }}}%</td>
                            <td class="center"><img data-toggle="tooltip" data-original-title="{{{ $position->user->supporting->name  }}}" data-placement="right" width="30px;" src="{{{ $position->user->supporting->flag_path }}}"></td>
                            <td class="center">{{{ $position->user->myCountry->name }}}</td>
                        </tr>
                        @endif
                        <?php $prevPoints = $position->scores->sum('points'); ?>
                        @endforeach
                    </tbody>
                </table>
                @else
            <h2 class="strong innerB half"><i class="fa fa-bolt text-primary icon-fixed-width"></i>Ranking Global de "{{{ $userInfo->user->first_name .' '.$userInfo->user->last_name }}}"</h2>
                <table class="table table-striped table-condensed margin-none">
                    <thead>
                        <tr>
                            <th class="center">Ranking</th>
                            <th class="center">Usuario</th>
                            <th class="center">Total Puntos</th>
                            <th class="center">Ptos Ganador/Empate</th>
                            <th class="center">Ptos Marcador</th>
                            <th class="center">Eficiencia</th>
                            <th class="center">Precisión</th>
                            <th class="center">Favorito</th>
                            <th class="center">País</th>
                        </thead>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $j=0; 
                          $prevPoints = 0;?>
                        @foreach($allUserInfo as $position)
                        @if($position->scores->sum('points') != $prevPoints)
                        <?php $j++; ?>
                        @endif
                        @if (Auth::user()->id == $position->user->id)
                        <tr>
                            <td class="center"><strong>{{{ $j }}}</strong></td>
                            <td class="center"><strong><a data-toggle="tooltip" data-original-title="De: {{{ $position->user->myCountry->name }}}" data-placement="right"  href="/user_scores/{{{ $position->user->id }}}">{{{ $position->user->username }}}</a></strong></td>
                            <td class="center"><strong>{{{ $position->total_points }}}</strong></td>
                            <td class="center"><strong>{{{ $position->scores->sum('winner_points') }}}</strong></td>
                            <td class="center"><strong>{{{ $position->scores->sum('score_points') }}}</strong></td>
                            <td class="center">{{{ (Game::getGamesPlayed() !=0)? number_format($position->scores->sum('points')/Game::getGamesPlayed()/2*100, 1): '0' }}}%</td>
                            <td class="center">{{{ (Game::getGamesPlayed() !=0)? number_format($position->scores->sum('score_points')/Game::getGamesPlayed()*100, 1) : '0' }}}%</td>
                            <td class="center"><strong><img data-toggle="tooltip" data-original-title="{{{ $position->user->supporting->name  }}}" data-placement="right" width="30px;" src="{{{ $position->user->supporting->flag_path }}}"></strong></td>
                            <td class="center"><strong>{{{ $position->user->myCountry->name }}}</strong></td>
                        </tr>
                        @else
                        <tr>
                            <td class="center">{{{ $j }}}</td>
                            <td class="center"><a data-toggle="tooltip" data-original-title="De: {{{ $position->user->myCountry->name }}}" data-placement="right"  href="/user_scores/{{{ $position->user->id }}}">{{{ $position->user->username }}}</a></td>
                            <td class="center">{{{ $position->total_points }}}</td>
                            <td class="center">{{{ $position->scores->sum('winner_points') }}}</td>
                            <td class="center">{{{ $position->scores->sum('score_points') }}}</td>
                            <td class="center">{{{ (Game::getGamesPlayed() !=0)? number_format($position->scores->sum('points')/Game::getGamesPlayed()/2*100, 1) : '0' }}}%</td>
                            <td class="center">{{{ (Game::getGamesPlayed() !=0)? number_format($position->scores->sum('score_points')/Game::getGamesPlayed()*100, 1) : '0' }}}%</td>
                            <td class="center"><img data-toggle="tooltip" data-original-title="{{{ $position->user->supporting->name  }}}" data-placement="right" width="30px;" src="{{{ $position->user->supporting->flag_path }}}"></td>
                            <td class="center">{{{ $position->user->myCountry->name }}}</td>
                        </tr>
                        @endif
                        <?php $prevPoints = $position->scores->sum('points'); ?>
                        @endforeach
                    </tbody>
                </table>
                @endif
                <div class="separator bottom"></div>
        </div>
    </div>
</div>
<div class="separator bottom"></div>
<div class="row border-top border-bottom innerLR">
    <div class="col-md-8">
        <div class="innerAll">
        @if ($lang == 'en')
            <h2 class="strong innerB half"><i class="fa fa-bolt text-primary icon-fixed-width"></i>Rankings in "{{{ $userInfo->user->myCountry->name }}}" for "{{{ $userInfo->user->first_name .' '.$userInfo->user->last_name }}}"</h2>
                <table class="table table-striped table-condensed margin-none">
                    <thead>
                        <tr>
                            <th class="center">Ranking</th>
                            <th class="center">Username</th>
                            <th class="center">Points</th>
                            <th class="center">Winner Pick Pts</th>
                            <th class="center">Exact Score Pts</th>
                            <th class="center">Efficiency</th>
                            <th class="center">Precision</th>
                            <th class="center">Supporting</th>
                        </thead>
                        </tr>
                    </thead>
                    <tbody>
                          <?php $j=0; 
                          $prevPoints = 0;?>
                        @foreach($allUserInfo as $position)
                         @if($position->scores->sum('points') != $prevPoints)
                        <?php $j++; ?>
                        @endif
                        @if ($userInfo->user->myCountry->name == $position->user->myCountry->name)
                        @if (Auth::user()->id == $position->user->id)
                        <tr>
                            <td class="center"><strong>{{{ $j }}}</strong></td>
                            <td class="center"><strong><a data-toggle="tooltip" data-original-title="From: {{{ $position->user->myCountry->name }}}" data-placement="right"  href="/user_scores/{{{ $position->user->id }}}">{{{ $position->user->username }}}</a></strong></td>
                            <td class="center"><strong>{{{ $position->total_points }}}</strong></td>
                            <td class="center"><strong>{{{ $position->scores->sum('winner_points')  }}}</strong></td>
                            <td class="center"><strong>{{{ $position->scores->sum('score_points')  }}}</strong></td>
                            <td class="center">{{{ (Game::getGamesPlayed() !=0)? number_format($position->scores->sum('points')/Game::getGamesPlayed()/2*100, 1): '0' }}}%</td>
                            <td class="center">{{{ (Game::getGamesPlayed() !=0)? number_format($position->scores->sum('score_points')/Game::getGamesPlayed()*100, 1) : '0' }}}%</td>
                            <td class="center"><strong><img data-toggle="tooltip" data-original-title="{{{ $position->user->supporting->name  }}}" data-placement="right" width="30px;" src="{{{ $position->user->supporting->flag_path }}}"></strong></td>
                        </tr>

                        @else
                        <tr>
                            <td class="center">{{{ $j }}}</td>
                            <td class="center"><a data-toggle="tooltip" data-original-title="From: {{{ $position->user->myCountry->name }}}" data-placement="right"  href="/user_scores/{{{ $position->user->id }}}">{{{ $position->user->username }}}</a></td>
                            <td class="center">{{{ $position->total_points }}}</td>
                            <td class="center">{{{ $position->scores->sum('winner_points')  }}}</td>
                            <td class="center">{{{ $position->scores->sum('score_points')  }}}</td>
                            <td class="center">{{{ (Game::getGamesPlayed() !=0)? number_format($position->scores->sum('points')/Game::getGamesPlayed()/2*100, 1): '0' }}}%</td>
                            <td class="center">{{{ (Game::getGamesPlayed() !=0)? number_format($position->scores->sum('score_points')/Game::getGamesPlayed()*100, 1) : '0' }}}%</td>
                            <td class="center"><img data-toggle="tooltip" data-original-title="{{{ $position->user->supporting->name  }}}" data-placement="right" width="30px;" src="{{{ $position->user->supporting->flag_path }}}"></td>
                        </tr>
                        @endif
                        @endif
                        <?php $prevPoints = $position->scores->sum('points'); ?>
                        @endforeach
                    </tbody>
                </table>
                <div class="separator bottom"></div>
			@else
            <h2 class="strong innerB half"><i class="fa fa-bolt text-primary icon-fixed-width"></i>Ranking en "{{{ $userInfo->user->myCountry->name }}}" de "{{{ $userInfo->user->first_name .' '.$userInfo->user->last_name }}}"</h2>
                <table class="table table-striped table-condensed margin-none">
                    <thead>
                        <tr>
                            <th class="center">Ranking</th>
                            <th class="center">Usuario</th>
                            <th class="center">Puntos</th>
                            <th class="center">Ptos Ganador/Empate</th>
                            <th class="center">Ptos Marcador</th>
                            <th class="center">Eficiencia</th>
                            <th class="center">Precisión</th>
                            <th class="center">Favorito</th>
                        </thead>
                        </tr>
                    </thead>
                    <tbody>
                         <?php $j=0; 
                          $prevPoints = 0;?>
                        @foreach($allUserInfo as $position)
                        @if ($userInfo->user->myCountry->name == $position->user->myCountry->name)
                          @if($position->scores->sum('points') != $prevPoints)
	                      <?php $j++; ?>
                          @endif
                            @if (Auth::user()->id == $position->user->id)
                                <tr>
                                    <td class="center"><strong>{{{ $j }}}</strong></td>
                                    <td class="center"><strong><a data-toggle="tooltip" data-original-title="De: {{{ $position->user->myCountry->name }}}" data-placement="right"  href="/user_scores/{{{ $position->user->id }}}">{{{ $position->user->username }}}</a></strong></td>
                                    <td class="center"><strong>{{{ $position->total_points }}}</strong></td>
                                    <td class="center"><strong>{{{ $position->scores->sum('winner_points')  }}}</strong></td>
                                    <td class="center"><strong>{{{ $position->scores->sum('score_points')  }}}</strong></td>
                                    <td class="center">{{{ (Game::getGamesPlayed() !=0)? number_format($position->scores->sum('points')/Game::getGamesPlayed()/2*100, 1) : '0' }}}%</td>
                                    <td class="center">{{{ (Game::getGamesPlayed() !=0)? number_format($position->scores->sum('score_points')/Game::getGamesPlayed()*100, 1) : '0' }}}%</td>
                                    <td class="center"><strong><img data-toggle="tooltip" data-original-title="{{{ $position->user->supporting->name  }}}" data-placement="right" width="30px;" src="{{{ $position->user->supporting->flag_path }}}"></strong></td>
                                </tr>
                            @else
                                <tr>
                                    <td class="center">{{{ $j }}}</td>
                                    <td class="center"><a data-toggle="tooltip" data-original-title="De: {{{ $position->user->myCountry->name }}}" data-placement="right"  href="/user_scores/{{{ $position->user->id }}}">{{{ $position->user->username }}}</a></td>
                                    <td class="center">{{{ $position->total_points }}}</td>
                                    <td class="center">{{{ $position->scores->sum('winner_points')  }}}</td>
                                    <td class="center">{{{ $position->scores->sum('score_points')  }}}</td>
                                    <td class="center">{{{ (Game::getGamesPlayed() !=0)? number_format($position->scores->sum('points')/Game::getGamesPlayed()/2*100, 1): '0' }}}%</td>
                                    <td class="center">{{{ (Game::getGamesPlayed() !=0)? number_format($position->scores->sum('score_points')/Game::getGamesPlayed()*100, 1) : '0' }}}%</td>
                                    <td class="center"><img data-toggle="tooltip" data-original-title="{{{ $position->user->supporting->name  }}}" data-placement="right" width="30px;" src="{{{ $position->user->supporting->flag_path }}}"></td>
                                </tr>
	                        @endif
                        @endif
                        <?php $prevPoints = $position->scores->sum('points'); ?>
                        @endforeach
                    </tbody>
                </table>
                <div class="separator bottom"></div>
	        @endif
        </div>
    </div>
</div>

@stop 