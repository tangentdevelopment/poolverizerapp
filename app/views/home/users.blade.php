@extends('layouts.master')

@section('content')

<script type="text/javascript">
	function confirmDelete() {
		var accept = confirm("Are you sure you want to delete this user?");
		if (accept) {
			return true;
		} else {
			return false;
		}
	}
</script>

<ul class="breadcrumb">
	<li>You are here</li>
	<li><a href="/dashboard" class="glyphicons dashboard"><i></i> POOLVERIZER</a></li>
			<li class="divider"><i class="fa fa-caret-right"></i></li>
		<li>Users</li>
	</ul>
				
	<div class="innerLR">
</div>



<div class="separator bottom"></div>

<div class="row innerLR">


	<div class="col-md-12">
		<div class="innerAll">
			<h1 class="strong innerB half"><i class="fa fa-group text-primary icon-fixed-width"></i> Users @if(Auth::user()->role == 3) in group {{{ $adminInfo->group->name }}} @endif </h1>
			<span class="pull-right">
				<a href="{{ action('UserController@create') }}" class="btn btn-block btn-success" type="button" style="width:150px;background-color:#CFB066;border-color:#CFB066;">New User</a>
			</span>
			<table class="table table-striped table-condensed margin-none">
				<thead>
					<tr>
						<th>Username</th>
						<th>Name</th>
						<th>Role</th>
						@if(Auth::user()->role == 3)
						@else
						<th>Group</th>
						@endif
						<th>From</th>
						<th>Paid</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr>
						<td>{{{ $user->username }}}</td>
						<td>{{{ $user->first_name.' '.$user->last_name}}}</td>
						<td>{{{ ($user->role != 1)? 'Admin' : 'User' }}}</td>
						@if(Auth::user()->role == 3)
						@else
						<td>{{{ $user->getGroup() }}}</td>
						@endif
						<td>{{{ $user->myCountry->name }}}</td>
						<td>{{{ ($user->payment_received != 1)? 'No' : 'Yes' }}}</td>
						<td>
							{{ Form::open(array('action' => array('UserController@destroy', $user->id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'onSubmit' => 'return confirmDelete()')) }}
										@if(Auth::user()->id != $user->id)
										<a href="{{ action('UserController@edit', $user->id) }}" class="btn btn-block btn-default" style="margin-right:20px;float:left;height:40px;width:60px;">Edit</a> 
										{{ Form::submit('Delete', array('class' => 'btn btn-block btn-primary', 'style' => 'margin-top:0px;float:left;height:40px;width:60px;')) }}
										@endif
							{{ Form::close() }}
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<div class="separator bottom"></div>
		</div>
	</div>
</div>

@stop 