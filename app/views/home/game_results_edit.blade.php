@extends('layouts.master')

@section('content')
				
<ul class="breadcrumb">
	<li>You are here</li>
	<li><a href="/dashboard" class="glyphicons dashboard"><i></i> POOLVERIZER</a></li>
	<li class="divider"><i class="fa fa-caret-right"></i></li>
	<li>Game Results</li>
</ul>
		
{{ Form::open(array('url' => '/game_results/edit')) }}		
<div class="innerLR">
	<div class="innerB">
		
		<h1 class="pull-left margin-none ">Edit Game Results</h1>
		@if(Auth::user()->id != 1)
				<a href="/user_scores/{{{  Auth::user()->id }}}" class="btn btn-default pull-right"><i class="fa fa-fw fa-user"></i>Your Scorecard</a>
		@endif

		@if(Auth::user()->id == 1)			
			
					<!-- <a id="edit-results" href="#" class="btn btn-block btn-default" style="margin-right:20px;float:left;height:40px;">Edit Results</a> 
					<a id="save-results" href="#" class="btn btn-block btn-success" style="margin-top:0px;float:left;height:40px;width:60px;display:none">Save</a> -->

			<a href="/game_results/" style="margin-right:4px;" class="btn btn-default pull-right"><i class="fa fa-fw  fa-times-circle"></i>Cancel</a>
			<button id="submit" action="submit" class="btn btn-info pull-right" style="margin-right:4px;"><i class="fa fa-fw  fa-save"></i>Save changes</button>			
			
			@endif

		<div class="clearfix">		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			* Hover over flag to see country's name
			

			
			<div class="separator"></div>
			<!-- Widget -->


			<div class="widget widget-body-white">
				<div class="widget-body">
						<div class="clearfix"></div>
					<!-- // Total bookings & sort by options END -->
						
					<!-- Table -->
					<table style="width:60%", class="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs">
						<thead>
							<tr>
								<th class="center">Group</th>
								<th class="center">Date</th>
								<th class="center">Time</th>
								<th class="center">Team 1</th>
								<th class="center">Goals</th>
								<th class="center">Goals</th>
								<th class="center">Team 2</th>
								<th class="center">Played</th>
								<!-- <th class="center">Admin</th> -->
							</tr>
						</thead>
						<tbody>
							@foreach($everyGame as $game)							<!-- Item -->
								@if($game->id==1)
								<tr>
									<td colspan="9"><h2>Group Stage</h2></td>
								</tr>
								@endif
								@if($game->id==49)
								<tr>
									<td colspan="9"><h2>Round of 16</h2></td>
								</tr>
								@endif
								@if($game->id==57)
								<tr>
									<td colspan="9"><h2>Quarter-Finals</h2></td>
								</tr>
								@endif
								@if($game->id==61)
								<tr>
									<td colspan="9"><h2>Semi-Finals</h2></td>
								</tr>
								@endif
								@if($game->id==63)
								<tr>
									<td colspan="9"><h2>Play-off for Third Place</h2></td>
								</tr>
								@endif
								@if($game->id==64)
								<tr>
									<td colspan="9"><h2>Final</h2></td>
								</tr>
								@endif
								<tr class="selectable">
									<td class="center">@if($game->id <49) Group {{{ Country::teamGroup($game->countryLocal->group)  }}}@endif</td>
									<td class="center">{{{ date('m/d/Y', strtotime($game->game_date)) }}}</td>
									<td class="center">{{{ date('H:i', strtotime($game->game_date)) }}}</td>
									<td class="center"><img data-toggle="tooltip" data-original-title="{{{ $game->countryLocal->name }}}" data-placement="left" width="40px" height="25px;" src="{{{ $game->countryLocal->flag_path }}}"></td>
									<!-- <td class="center" style="font-size:24px;">{{{ $game->country1_goals }}}</td>
									<td class="center" style="font-size:24px;">{{{ $game->country2_goals }}}</td> -->

									<td class="center" style="font-size:24px;"><input name="gameData[{{{ $game->id }}}][country1_goals]" style="width:70px;" type="number" class="form-control" value="{{{ $game->country1_goals }}}"></td>
									<td class="center" style="font-size:24px;"><input name="gameData[{{{ $game->id }}}][country2_goals]" style="width:70px;" type="number" class="form-control" value="{{{ $game->country2_goals }}}"></td>

									<td class="center"><img data-toggle="tooltip" data-original-title="{{{ $game->countryVisit->name }}}" data-placement="right"width="40px" height="25px;" src="{{{ $game->countryVisit->flag_path }}}"></td>
									<td class="center">										
										@if($game->played)
											<input type="checkbox" name="gameData[{{{ $game->id }}}][played]" value="1" checked="checked">
										@else											
											<input type="checkbox" name="gameData[{{{ $game->id }}}][played]" value="1">											
										@endif

									</td>
								</tr>
							@endforeach													
						</tbody>
					</table>
					<!-- // Table END -->													

				</div>  
				<!-- //end widget body -->
			</div>  
			<!-- //End Widget -->
		</div>		
	</div>
	<!-- End Row	 -->						
</div>
<!-- End inner -->
{{ Form::close() }}

	<!-- // Main Container Fluid END -->

@stop

<script type="text/javascript">


</script>


