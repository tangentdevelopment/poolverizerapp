@extends('layouts.master')

@section('content')
				
<ul class="breadcrumb">
@if ($lang == 'en')
	<li>You are here</li>
	<li><a href="/dashboard/{{{ $lang }}}" class="glyphicons dashboard"><i></i> POOLVERIZER</a></li>
			<li class="divider"><i class="fa fa-caret-right"></i></li>
		<li>Categorías Premiadas</li>
@else
	<li>Estás aquí</li>
	<li><a href="/dashboard/{{{ $lang }}}" class="glyphicons dashboard"><i></i> POOLVERIZER</a></li>
			<li class="divider"><i class="fa fa-caret-right"></i></li>
		<li>Awarded Categories</li>
@endif
	</ul>
				
	<div class="innerLR">
	<div class="innerB">
    @if ($lang == 'en')
		<h1 class="pull-left margin-none ">Awarded Categories</h1>
		@if(Auth::user()->id != 1)
				<a href="/user_picks/{{{  Auth::user()->id }}}" class="btn btn-default pull-right"><i class="fa fa-fw fa-user"></i>Your Picks</a>
		@endif
	</div>
	<div class="row">
		<div class="col-md-12">
			@if(Auth::user()->id == 1)																		
				<a href="/game_results/edit" class="btn btn-default pull-right" style="margin-right:4px;"><i class="fa fa-fw fa-edit"></i>Edit Picks</a>
				<a href="/game_results/update_points" class="btn btn-defuault btn-primary pull-right" style="margin-right:4px;">Calculate Points</a>

				<!-- Todo - Maybe calculate this in an Ajax call? Instead of going to a different view-->
				<!-- <a id="calculate-points" class="btn btn-defuault btn-primary pull-right" style="margin-right:4px;">Calculate Points</a> -->
			@endif
    @else
    		<h1 class="pull-left margin-none ">Categorías Premiadas</h1>
		@if(Auth::user()->id != 1)
				<a href="/user_picks/{{{  Auth::user()->id }}}" class="btn btn-default pull-right"><i class="fa fa-fw fa-user"></i>Tus Predicciones</a>
		@endif
	</div>
	<div class="row">
		<div class="col-md-12">
			@if(Auth::user()->id == 1)																		
				<a href="/cats_awarded_entry" class="btn btn-default pull-right" style="margin-right:4px;"><i class="fa fa-fw fa-edit"></i>Introducir Resultados</a>
 				<!-- <a href="/game_results/final" class="btn btn-default pull-right" style="margin-right:4px;"><i class="fa fa-fw fa-edit"></i>Generar final</a> -->
				<!-- Todo - Maybe calculate this in an Ajax call? Instead of going to a different view-->
				<!-- <a id="calculate-points" class="btn btn-defuault btn-primary pull-right" style="margin-right:4px;">Calculate Points</a> -->
			@endif
			<div class="separator"></div>
    @endif

			<!-- Widget -->

			<div class="widget widget-body-white">
				<div class="widget-body">
					<!-- // Total bookings & sort by options END -->
						
					<!-- Table -->
				
					@foreach($allCategories as $category)
					<table style="width:80%", class="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs">
						<thead>
							<tr><th colspan="2"><h3>@if ($lang == 'en'){{{ $category->name }}}@else {{{ $category->name_sp }}} @endif<span style="font-size:14px;">- {{{ $category->points }}} Pts</span></h3></th></tr>
						</thead>
						<tbody>
							@foreach($category->nominees as $nominee)
							<tr><td width="70%">{{{ $nominee->name }}}</td>
							<td witdth="30%">@if($nominee->winner == 1) <label class="label label-success">Winner!!!</label> @endif</td></tr>
							@endforeach
						</tbody>
					</table>
					@endforeach
					<!-- // Table END -->													

				</div>  
				<!-- //end widget body -->
			</div>  
			<!-- //End Widget -->
		</div>
	</div>
	<!-- End Row	 -->						
</div>
<!-- End inner -->

	<!-- // Main Container Fluid END -->

@stop


