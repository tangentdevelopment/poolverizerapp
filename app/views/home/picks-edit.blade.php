@extends('layouts.master')

@section('content')
				
<ul class="breadcrumb">
	<li>You are here</li>
	<li><a href="/dashboard" class="glyphicons dashboard"><i></i> POOLVERIZER</a></li>
			<li class="divider"><i class="fa fa-caret-right"></i></li>
		<li><a href="/user_scores/{{{ $thisUser->id }}}">Picks</a></li>
		<li class="divider"><i class="fa fa-caret-right"></i></li>
		<li>Edit</li>
	</ul>
				
	<div class="innerLR">
	<div class="innerB">
	   {{ Form::open(array('id' => 'savePicksForm', 'url' => '/save_user_picks')) }}
		<h1 class="pull-left margin-none ">
			@if ($lang == 'en')Enter your picks @else Tus Predicciones @endif</h1>
			@if ($lang == 'en')
			<a href="/cats_awarded" class="btn btn-default pull-right" style="margin-left:5px;"><i class="fa fa-fw fa-trophy"></i>Awarded Categories</a>
            @else
  
   			<a href="/cats_awarded" class="btn btn-default pull-right" style="margin-left:5px;"><i class="fa fa-fw fa-trophy"></i>Categorías Premiadas</a>
            @endif
				<a href="/dashboard" class="btn btn-warning pull-right"><i class="fa fa-fw  fa-ban"></i>@if ($lang == 'en')Cancel @else Cancelar @endif</a>
				<a id="submitPicks"  class="btn btn-info pull-right"><i class="fa fa-fw  fa-save"></i>@if ($lang == 'en')Save changes @else Guardar @endif</a>
		<div class="clearfix">		</div>
	</div>
	<div class="row">
		<div class="col-md-12">						<!-- Widget -->

			<div class="widget widget-body-white">
				<div class="widget-body">
					<!-- // Total bookings & sort by options END -->
						
					<!-- Table -->
					<div class="alert alert-info"><span style="font-size:14;color:gray"><b>@if ($lang == 'en') Your predictions will lock after you save. So, choose wisely!! @else Tus predicciones no podrán ser modificadas una vez que las guardes. Elige sabiamente!!@endif</b></span></div>				<!-- // Table END -->		
					@foreach($allCategories as $category)
					<table style="width:80%", class="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs">
						<thead>
							<tr><th colspan="2"><h3>@if ($lang == 'en'){{{ $category->name }}}@else {{{ $category->name_sp }}} @endif<span style="font-size:14px;">- {{{ $category->points }}} Pts</span></h3></th></tr>
						</thead>
						<tbody>
							<?php
								$order = 0;
							?>
							@foreach($category->nominees as $nominee)
							<tr><td width="70%">{{{ $nominee->name }}}</td>
							<td align="center " witdth="30%">
								<input type="radio" name="{{{ $category->id }}}" value="{{{ $nominee->id }}}" @if($order==0) checked @endif>
							</td></tr>
							<?php
								$order = $order+1;
							?>
							@endforeach
						</tbody>
					</table>
					@endforeach											
				</div>  
				<!-- //end widget body -->
			</div>  
			<!-- //End Widget -->
			</div>  
			<!-- //End Widget -->
		</div>
		  {{ Form::close() }}
	<!-- End Row	 -->						
</div>
<!-- End inner -->
	
	<!-- // Main Container Fluid END -->

@stop

@section('bottomscript')

<script src="/js/sweet-alert.min.js"></script>

<link rel="stylesheet" type="text/css" href="/css/sweet-alert.css">

	<script>

	$('#submitPicks').click(function(){
		swal({   title: "Are you Sure?",   text: "You won't be able to change your picks after this!!",    type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, save them!",   closeOnConfirm: false },
      	function(){
        $("#savePicksForm").submit();
    	});
	});

	// $(function(){
	//   var _isDirty = false;
	//   console.log(_isDirty);

	//   $(':input').change(function(){
	//     _isDirty = true;    
	//     console.log(_isDirty);
	//   });

	//   $('#submit').click(function(){
	//   	console.log('submit');
	//     _isDirty = false;
	//   });

	//   function closeEditorWarning(){
	//     if(_isDirty){
	//       return 'It looks like you have made some changes. Your changes will be lost if you leave before saving.';
	//     }else{
	//       return null;
	//     }    
	//   }

	//   window.onbeforeunload = closeEditorWarning;
	// });


	</script>

@stop

