@extends('layouts.master')

@section('content')

<ul class="breadcrumb">
	<li>You are here</li>
	<li><a href="index.html?lang=en" class="glyphicons dashboard"><i></i> POOLVERIZER</a></li>
			<li class="divider"><i class="fa fa-caret-right"></i></li>
		<li>Rules</li>
	</ul>
				
	<h1>Rules <span></span></h1>
<div class="innerLR">

	<!-- Widget -->
<div class="widget widget-tabs widget-tabs-double-2 widget-tabs-gray">

	<!-- Widget heading -->
	<div class="widget-head">
		<ul>
			<li class="active"><a class="glyphicons soccer_ball" href="#tabAll" data-toggle="tab"><i></i><span>General Rules</span></a></li>
			<li><a class="glyphicons cup" href="#tabAccount" data-toggle="tab"><i></i><span>Prizes</span></a></li>
			<li><a class="glyphicons warning_sign" href="#tabPayments" data-toggle="tab"><i></i><span>Disclaimer</span></a></li>
		</ul>
	</div>
	<!-- // Widget heading END -->
	
	<div class="widget-body">
		<div class="tab-content">
		
			<!-- Tab content -->
			<div id="tabAll" class="tab-pane active">
				<div class="panel-group accordion accordion-2" id="accordion">
					
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title"><a class="accordion-toggle glyphicons circle_question_mark" data-toggle="collapse" data-parent="#accordion" href="#collapse-1"><i></i>How to participate?</a></h4>
					    </div>
					    <div id="collapse-1" class="panel-collapse collapse in">
					      	<div class="panel-body">
					        	I.- Para participar se requiere donativo de $40 dólares. A cambio de ese donativo los organizadores de esta quiniela (el "Organizador”) se compromete a otorgar al donador (el “Participante”), sujeto al cumplimiento de las presentes REGLAS, un derecho a participar en Los Premios como se define en las reglas II y IV (Los “Premios”). <br>

<br>II.- Los Premios: <br>
<div style="padding-left:100px;">
	<ul>
		<li>1º. 60% de la Bolsa Acumulada</li>
		<li>2º. 25% de la Bolsa Acumulada</li>
		<li>3º. 15% de la Bolsa Acumulada</li>
	</ul>
</div>
<br>
La "Bolsa Acumulada" será la cantidad en dólares exactamente igual a la suma de todos los donativos recibidos de los Participantes. No habrá cobros por comisiones de ninguna especie para cualquier Participante o para el Organizador.

<br><br>III.- Solo tendrán derecho a participar en los Premios aquellos que otorguen el donativo completo. No hay fecha límite para hacer el donativo, sin embargo, aquellos que den su donativo una vez iniciado el torneo sólo podrán acumular puntos por los partidos que no se han jugado (no es retroactivo!). El Participante tiene la obligación de enviar la fase correspondiente al menos dos (2) horas antes de que inicie dicha fase, o bien si ya inicio la fase, al menos dos horas antes de que inicie la siguiente ronda de partido(s). No se podrán revisar las predicciones de los partidos o enviarlas parcialmente durante el transcurso de la fase correspondiente. Una vez enviada la fase respectiva al Organizador, no se admitirán correcciones, salvo que dicha fase no haya iniciado. En el caso de sustituciones, es responsabilidad del Participante interesado asegurase de que la fase corregida llegue a tiempo al Organizador. Si hubiera partidos sin marcador durante la fase remitida al Organizador, se asumirá que el Participante predice empate (0,0) en ese partido. En tanto no se haya enviado el archivo con la quiniela de la fase correspondiente, los partidos transcurridos no acumularán puntos, por lo que no se asumirá el resultado (0,0) como en el caso donde se omitió alguna predicción.

<br><br>IV.- Para definir Los Premios, los puntos se acumularán así: 

<br><br>
<h4>Tabla de Puntos</h4>
<table style="width:50%"  class="table table-striped table-condensed margin-none">
	<thead>
	<tr>
		<th class="center">Partidos</th>
		<th class="center">Marcador General (Ganó, Perdió, Empató)</th>
		<th class="center">Marcador en Goles</th>
		<th class="center">Peso Ponderado(*)</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td class="center">Fase de Grupos</td>
		<td class="center">1</td>
		<td class="center">1</td>
		<td class="center">42%</td>
	</tr>
		<tr>
		<td class="center">Octavos</td>
		<td class="center">2</td>
		<td class="center">2</td>
		<td class="center">14%</td>
	</tr>
		<tr>
		<td class="center">Cuartos</td>
		<td class="center">4</td>
		<td class="center">4</td>
		<td class="center">14%</td>
	</tr>
		<tr>
		<td class="center">Semifinal</td>
		<td class="center">8</td>
		<td class="center">8</td>
		<td class="center">14%</td>
	</tr>
	<tr>
		<td class="center">3o - 4o</td>
		<td class="center">6</td>
		<td class="center">6</td>
		<td class="center">5%</td>
	</tr>
	<tr>
		<td class="center">Final</td>
		<td class="center">10</td>
		<td class="center">12</td>
		<td class="center">10%</td>
	</tr>
</tbody>
</table><br>
<span style="font-size:10px;">(*) Lo que el puntaje disponible por fase representa en el total del torneo</span>
<br>
<br>

Esta tabla de puntos será la base para el cálculo del puntaje en cada fase. Para los partidos de la fase de octavos y subsecuentes que se resuelvan en penalties, el marcador para efectos del puntaje será el resultado al final de los tiempos extras, por lo que es posible que exista el empate. 

<br><br>V.- La quiniela es sobre todo el torneo. Sin embargo los participantes enviarán sus predicciones completas para cada una de las fases utilizando poolverizer.com para editar sus "ScoreCards" de esta Quiniela Russia 2018. 

<br><br>VI.- El donativo se otorgará al Organizador, quien rendirá cuentas claras de los Participantes, de manera que todo mundo pueda calcular la Bolsa Acumulada. Sólo cuando el donativo haya sido recibido por el Organizador, el interesado se contará como Participante para efectos del cálculo de la Bolsa Acumulada. El donativo se podrá enviar vía Pay Pal o directamente con el Organizador. Comunícate con el Organizador a su correo pacda98@yahoo.com para los detalles. 

<br><br>VII.- El Organizador se compromete a entregar los Premios en los siguientes cuatro días hábiles posteriores al partido final del mundial Russia 2018 vía deposito en su cuenta de Pay Pal, o bien se les enviará un cheque o giro postal. Cualquier gasto administrativo por la transferencia se restarán del premio correspondiente, mostrando la documentación correspondiente al Participante.

<br><br>VIII.- Los Participantes aceptan que todas las quinielas de los Participantes serán de información accesible para el resto de los Participantes, por lo que el Organizador las compartirá en archivos de pdf, a fin de que cualquiera pueda auditar los puntos de los demás participantes. Los Participantes podrá usar un seudónimo para identificarse en la tabla de resultados. 

<br><br>IX.- Cualquier omisión en las reglas será resuelta por el Organizador de manera inapelable, previa explicación a los Participantes.
					      	</div>
					    </div>
				  	</div>
				</div>
			</div>
			<!-- // Tab content END -->
			
			<!-- Tab content -->
			<div id="tabAccount" class="tab-pane">
			
				<!-- Accordion -->
				<div class="panel-group accordion accordion-2" id="tabAccountAccordion">
					
					<!-- Accordion Group -->
					<div class="panel panel-default">
					
						<!-- Heading -->
						<div class="panel-heading">
							<h4 class="panel-title"><a class="accordion-toggle glyphicons circle_question_mark" data-toggle="collapse" data-parent="#tabAccountAccordion" href="#collapse-1-1"><i></i>What are the prizes?</a></h4>
					    </div>
					    <!-- // Heading END -->
					    
					    <!-- Body -->
					    <div id="collapse-1-1" class="panel-collapse collapse in">
					      	<div class="panel-body">
					        	1o. 60% of Total Donations<br>
								2o. 25% of Total Donations<br>
								3o. 15% of Total Donations<br>
					      	</div>
					    </div>
					    <!-- // Body END -->
					    
				  	</div>
				  	<!-- // Accordion Group END -->
				  	
				  	<!-- Accordion Group -->
				  	<div class="panel panel-default">
				  	
				  		<!-- Heading -->
					    <div class="panel-heading">
					      	<h4 class="panel-title"><a class="accordion-toggle glyphicons circle_question_mark" data-toggle="collapse" data-parent="#tabAccountAccordion" href="#collapse-2-1"><i></i>How can I be entitled to a prize?</a></h4>
					    </div>
					    <!-- // Heading END -->
					    
					    <!-- Body -->
					    <div id="collapse-2-1" class="panel-collapse collapse">
					    	<div class="panel-body">
					        	1.- Once invited as a participant by the Administrator, send out your donation of $40 either by <a href="https://www.paypal.com/us/webapps/mpp/home">Pay Pal</a> or directly with the Administrator to complete de prizes. Email to <a href="mailto:pacda98@yahoo.com ">pacda98@yahoo.com</a> for details.

								<br><br>2.- Fill out your pool for the corresponding stage of the tournament using the "Edit Scorecard" functionality. Check out the points table for your strategy. 

								<br><br>3.- Submit your "Scorecard" with at least 2 hours prior the start of the corresponding stage, that is, Group Stage, Round of 16, Quarter Final, Semi Final, and Final. No partially filled stages will be accepted. If the pool is not submitted this way, it will automatically forego the points contested within the next two hours, and so will it subsequently until it is submitted to the Administrator. 

								<br><br>4.- All pools submitted will be available in a pdf format to all participants at the start of each stage or as soon as it is submitted, so that everyone is able to check other participant's pools <a href="/dashboard">here</a>.

								<br><br>5.- Prizes will be delivered within four days after the end of the World Cup. Any fees charged by the delivery method will be deducted and paid out of the corresponding prize.
					      	</div>
					    </div>
					    <!-- // Body END -->
					    
				  	</div>				  	
				</div>
			</div>
			<!-- // Tab content END -->
			
			<!-- Tab content -->
			<div id="tabPayments" class="tab-pane widget-body-regular">
				<h5>Disclaimer</h5>
				<p>ESTA QUINIELA DEL MUNDIAL ES EXCLUSIVAMENTE PRIVADA, ENTRE AMIGOS. EL ORGANIZADOR INVITARÁ PERSONALMENTE A LOS PARTICIPANTES. NO ES PÚBLICA, POR LO QUE EL ORGANIZADOR SE RESERVA TODO DERECHO DE ACEPTAR CUALQUIER PARTICIPANTE, POR CUALQUIER MOTIVO. NO TIENE FINES DE LUCRO NI FINES COMERCIALES, POR LO QUE NINGUNO DE LOS PARTICIPANTES, NI EL (LOS) ORGANIZADOR(ES), RECIBIRÁN BENEFICIO ECONÓMICO PERSONAL DISTINTO A LOS PREMIOS QUE SE OBTENGAN CONFORME A LAS REGLAS. LOS PREMIOS SE CONSTITUYEN CON LA TOTALIDAD DE LOS DONATIVOS DE LOS PARTICIPANTES Y SE DISTRIBUIRÁN CONFORME A LOS PORCENTAJES AQUI PRESENTADOS Y CONFORME A LAS REGLAS. NO SE ACEPTAN PARTICIPACIONES CON FINES DE APUESTAS, RIFAS O SORTEOS. NI ESTA PAGINA, NI EL ORGANIZADOR ACTÚAN EN NOMBRE DE ALGUNA INSTITUCIÓN U ORGANIZACIÓN, SINO A TÍTULO PERSONAL.</p>
			</div>
			<!-- // Tab content END -->
			<!-- // Tab content END -->
			</div>
		</div>
	</div>
</div>
<!-- // Widget END -->


	
<p class="separator text-center"><i class="fa fa-phone fa-3x"></i></p>

<h3>Contact</h3>
<div class="row border-bottom border-top">
	<div class="col-md-7">
	<div class="col-md-12">
		<div class="innerAll">
			<div class="well margin-none">
				<address class="margin-none">
					<h2>David Diaz</h2>
					<strong>Organizator</strong> at 
					<strong><a href="#">www.poolverizer.com and www.wcup2014brazil.com</a></strong><br> 
					<abbr title="Work email">e-mail:</abbr> <a href="mailto:pacda98@yahoo.com">pacda98@yahoo.com</a><br /> 
					<div class="separator line"></div>
				</address>
			</div>
		</div>
</div>
	
	
		
		</div>
		<!-- // Content END -->
	</div>

@stop 