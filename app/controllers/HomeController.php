<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	public function showLogin()
	{
		//$user = new User;
		//return View::make('home.login')->with('user', $user);
		return View::make('home.login');
	}

	public function showIndex()
	{
		//$user = new User;
		//return View::make('home.login')->with('user', $user);
		return View::make('home.index');
	}

	public function showRegister()
	{
		$countries = Country::orderBy('name', 'ASC')->get();
		$groups = Group::orderBy('id', 'ASC')->get();
		$userCountries = Country::where('name', '=', 'United States')->orwhere('name', '=', 'Mexico')->orderBy('name', 'ASC')->get();
		return View::make('home.register')->with(array('countries'=>$countries, 'groups'=>$groups, 'userCountries'=>$userCountries));
	}


	public function doRegister()
	{

		$validator = Validator::make($data = Input::all(), User::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$user = new User();
		$user->username = Input::get('username');
		$user->first_name = Input::get('first_name');
		$user->last_name = Input::get('last_name');
		$user->email = Input::get('email');
		$user->password = Input::get('password');
		$user->payment_received = intval(Input::get('payment_received'));
		$user->country = Input::get('country');
		$user->country_supported = Input::get('country_supported');
		$user->role = 1;
		$user->save();

		$userGroup = new UserGroup();
		$userGroup->user_id = $user->id; 
		$userGroup->group_id = Input::get('group');
		$userGroup->save();

		for($p=1; $p<49; $p++){
				$thisScore = new Score();
				$thisScore->user_group_id = $userGroup->id;
				$thisScore->game_id = $p;
				$thisScore->country1_goals = 0;
				$thisScore->country2_goals = 0;
				$thisScore->save();
		}

		return Redirect::action('HomeController@showLogin');
	}

	public function doLogin()
	{
		$username = Input::get('username');
		$password = Input::get('password');
		$remember = true;

		if (Auth::attempt(array('username' => $username, 'password' => $password), $remember))
		{
            $lang = (Auth::user()->country == 3)? 'sp' : 'en';
            Session::put('lang', $lang);
            return Redirect::action('HomeController@showDashboard')->with('message', 'You have logged in successfully');
		}

		return Redirect::action('HomeController@showLogin')->with('loginError', 'Invalid username / password combination.')->withInput();
	}

	public function logout()
	{
		Auth::logout();
		return Redirect::action('HomeController@showLogin');
	}

	public function showDashboard()
	{
		$lang = Session::get('lang');
		//$user = new User;
		//return View::make('home.login')->with('user', $user);
		if(Auth::user()->id == 1){
			$userInfo = '';
			$allUserInfo = UserGroup::orderBy('total_points', 'DESC')->get();
			$groupAdmin = '';
			$ranking = 0;
		}else{
			$userInfo = UserGroup::where('user_id', '=', Auth::user()->id)->first();
			$allUserInfo = UserGroup::where('group_id', '=', $userInfo->group_id )->orderBy('total_points', 'DESC')->get();

			$ranking = 0;
			$j = 0;
			$prevPoints = 0;

			foreach($allUserInfo as $position){
				if($prevPoints != $position->total_points){
					$j++;
				}	
				if($position->user_id == Auth::user()->id){
					$ranking = $j;
				}
				$prevPoints = $position->total_points;
			}

			$groupAdmin = User::select('users.*')->leftJoin('user_groups', 'user_groups.user_id', '=', 'users.id')->where('users.role', '=', 3)->where('user_groups.group_id', '=', $userInfo->group_id )->first();
		}

		if(Auth::user()->id == 1){
			$totalMoney = 0;
			$allGroups = Group::get();
			foreach($allGroups as $group){
				$totalMoney += $group->numberOfMembers() * $group->contribution;
			}
		}else{
			$totalMoney = count($allUserInfo) * $userInfo->group->contribution;
		}

		return View::make('home.dashboard')->with(array('userInfo' => $userInfo, 'allUserInfo' => $allUserInfo, 'totalMoney' => $totalMoney, 'groupAdmin' => $groupAdmin, 'lang' => $lang, 'ranking' => $ranking ));
	}

	public function showUserScores($id)
	{
		$lang = Session::get('lang');
		$thisUser = User::find($id);
		$yourGroup = UserGroup::where('user_id', '=', Auth::user()->id )->first();

		$userGroup = UserGroup::where('user_id', '=', $id)->first();
		
		if(Auth::user()->id == 1){
			$everyGame = Score::where('user_group_id', '=', $userGroup->id)->get();
			return View::make('home.user_picks')->with(array('everyGame'=>$everyGame, 'thisUser'=> $thisUser, 'userGroup' => $userGroup, 'lang' => $lang));
		}
		else if($yourGroup->group_id != $userGroup->group_id){
			return Redirect::action('HomeController@showDashboard');
		}
		else
		{
			$everyGame = Score::where('user_group_id', '=', $userGroup->id)->get();
			return View::make('home.user_picks')->with(array('everyGame'=>$everyGame, 'thisUser'=> $thisUser, 'userGroup' => $userGroup, 'lang' => $lang));
		}
	}

	public function doUserScores($id)
	{
		$thisUser = User::find($id);
		
		if(!$thisUser){
			return Redirect::action('HomeController@showDashboard');
		}
		elseif($thisUser->id != Auth::user()->id)
		{
			return Redirect::action('HomeController@showUserScores', $id);
		}
		
		$gameInfoArray = array();
		$gameInfo = Input::get('formData');

		//print_r($gameInfo);

		$gameInfoArray = $gameInfo;

		foreach($gameInfoArray as $key => $score){
			$thisScore = Score::find($key);
            $thisScore->country1_goals = $score['country1_goals'];
            $thisScore->country2_goals = $score['country2_goals'];
            $thisScore->locked = 1;

            if(strtotime(date("Y-m-d H:i:s", strtotime('-3 hours', time()))) >=  strtotime(date('Y-m-d H:i:s', strtotime($thisScore->game->game_date)))){
            	$thisScore->ignore = 1;
            }
            
            $thisScore->save();
            
		 }

        return Redirect::action('HomeController@showUserScores', $id);
	}

	public function editUserScores($id)
	{
		$lang = Session::get('lang');
		$thisUser = User::find($id);
		
		if(!$thisUser){
			return Redirect::action('HomeController@showDashboard');
		}
		elseif($thisUser->id != Auth::user()->id)
		{
			return Redirect::action('HomeController@showUserScores', $id);
		}
		
		$userGroup = UserGroup::where('user_id', '=', $id)->first();
		$everyGame = Score::where('user_group_id', '=', $userGroup->id)->get();

		return View::make('home.picks-edit')->with(array('everyGame'=>$everyGame, 'thisUser'=> $thisUser, 'userGroup' => $userGroup, 'lang' => $lang));
	}

	public function showRules()
	{
		$lang = Session::get('lang');		
		//$user = new User;
		//return View::make('home.login')->with('user', $user);
		return View::make('home.rules')->with(array('lang'=>$lang));
	}

	public function showGameResults()
	{
		$lang = Session::get('lang');		
		//$user = new User;
		//return View::make('home.login')->with('user', $user);

		$everyGame = Game::get();

		return View::make('home.game_results')->with(array('everyGame'=>$everyGame, 'lang' => $lang));
	}

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function showRankings()
	{
		$lang = Session::get('lang');
		//$user = new User;
		//return View::make('home.login')->with('user', $user);
		if(Auth::user()->id == 1){
			$userInfo = '';
			$allUserInfo = UserGroup::orderBy('total_points', 'DESC')->get();
			$groupAdmin = '';
			$ranking = 0;
		}else{
			$userInfo = UserGroup::where('user_id', '=', Auth::user()->id)->first();
			$allUserInfo = UserGroup::orderBy('total_points', 'DESC')->get();
			
			$ranking = 0;
			$j = 0;
			$prevPoints = 0;

			foreach($allUserInfo as $position){
				if($prevPoints != $position->total_points){
					$j++;
				}	
				if($position->user_id == Auth::user()->id){
					$ranking = $j;
				}
				$prevPoints = $position->total_points;
			}

			$groupAdmin = User::select('users.*')->leftJoin('user_groups', 'user_groups.user_id', '=', 'users.id')->where('users.role', '=', 3)->where('user_groups.group_id', '=', $userInfo->group_id )->first();
		}

		return View::make('home.rankings')->with(array('userInfo' => $userInfo, 'allUserInfo' => $allUserInfo, 'groupAdmin' => $groupAdmin, 'lang' => $lang, 'ranking'=>$ranking));
	}
	
	public function showDashboardLang($lang)
	{
		Session::put('lang', $lang);
		//$user = new User;
		//return View::make('home.login')->with('user', $user);
		if(Auth::user()->id == 1){
			$userInfo = '';
			$allUserInfo = UserGroup::orderBy('total_points', 'DESC')->get();
			$groupAdmin = '';
			$ranking = 0;
		}else{
			$userInfo = UserGroup::where('user_id', '=', Auth::user()->id)->first();
			$allUserInfo = UserGroup::where('group_id', '=', $userInfo->group_id )->orderBy('total_points', 'DESC')->get();
			
			$ranking = 0;
			$j = 0;
			$prevPoints = 0;

			foreach($allUserInfo as $position){
				if($prevPoints != $position->total_points){
					$j++;
				}	
				if($position->user_id == Auth::user()->id){
					$ranking = $j;
				}
				$prevPoints = $position->total_points;
			}


			$groupAdmin = User::select('users.*')->leftJoin('user_groups', 'user_groups.user_id', '=', 'users.id')->where('users.role', '=', 3)->where('user_groups.group_id', '=', $userInfo->group_id )->first();
		}

		if(Auth::user()->id == 1){
			$totalMoney = 0;
			$allGroups = Group::get();
			foreach($allGroups as $group){
				$totalMoney += $group->numberOfMembers() * $group->contribution;
			}
		}else{
			$totalMoney = count($allUserInfo) * $userInfo->group->contribution;
		}

		return View::make('home.dashboard')->with(array('userInfo' => $userInfo, 'allUserInfo' => $allUserInfo, 'totalMoney' => $totalMoney, 'groupAdmin' => $groupAdmin, 'lang' => $lang, 'ranking'=>$ranking));
	}

	public function editGameResults(){
		$lang = Session::get('lang');

		$everyGame = Game::get();

		return View::make('home.game_results_edit')->with(array('everyGame'=>$everyGame, 'lang' => $lang));
	}

	public function doGameResults(){		
		$games = Input::get('gameData');

		foreach ($games as $key => $game) {
			$thisGame = Game::find($key);			
			$thisGame->country1_goals = $game['country1_goals'];
			$thisGame->country2_goals = $game['country2_goals'];
			
			if(isset($game['played'])){
				$thisGame->played = $game['played'];
			}else{
				$thisGame->played = 0;
			}
			
			$thisGame->save();
		}

		// $everyGame = Game::get();
		// return View::make('home.game_results')->with(array('everyGame'=>$everyGame));

		return Redirect::action('HomeController@showGameResults');
	}

	public function calculatePointsAll(){
		//Get only games that have been played
		$games = Game::where('played', 1)->get();		
		$ponderation = 1;

		foreach ($games as $game) {
			echo '***' . $game->id . ' -> ';
			echo $game->country1_goals;
			echo ' - ';
			echo $game->country2_goals;
			echo '<br>';
			$scoreCards = Score::where('game_id', $game->id)->get();
			
			//Get match result: Local won(1), Visitor won(2) or draw(3)
			$matchResult = self::getMatchResult($game->country1_goals, $game->country2_goals);
			$ponderationWinner = self::getPonderationWinner($game->id);
			$ponderationScore = self::getPonderationScore($game->id);
			//Todo - Get ponderation for the Final game, it's different for Exact Score

			foreach ($scoreCards as $score) {
				if($score->ignore==1){
					$score->points = 0;
					$score->winner_points = 0;
					$score->score_points = 0;

					$score->save();

				}else{
					$poolResult = self::getMatchResult($score->country1_goals, $score->country2_goals);

					//Check if the pool result is the same as the match
					if($matchResult == $poolResult){										
						$score->winner_points = $ponderationWinner;

						if(self::isScoreExact($game->country1_goals, $game->country2_goals, $score->country1_goals, $score->country2_goals)){						
							$score->score_points = $ponderationScore;
						}

						$score->points = $score->winner_points + $score->score_points;

					}else{
						$score->points = 0;
						$score->winner_points = 0;
						$score->score_points = 0;
					}

					$score->save();

					echo $score->id . ' -> ';
					echo $score->country1_goals;
					echo ' - ';
					echo $score->country2_goals;
					echo ' Winner Pts: ' . $score->winner_points . ' | Score Pts: ' . $score->score_points . ' | Total Pts: ' . $score->points;
					echo '<br>';
				}
				
			}
			
		}

		//Update users points
		self::updatePointsAllUsers();

	}

	public function updatePointsAllUsers(){
		$users = UserGroup::get();

		foreach ($users as $key => $user) {			
			$user->total_points = Score::where('user_group_id', $user->id)->sum('points');
			$user->save();
		}
	}

	//Calculate points for a single match
	public function calculatePoints($gameId){
		//To do

	}

	private function getMatchResult($localGoals, $visitorGoals){
		if($localGoals > $visitorGoals){
			return Game::LOCAL_WON;
		}elseif($localGoals < $visitorGoals){
			return Game::VISITOR_WON;
		}else{
			return Game::DRAW;
		}
	}

	private function isScoreExact($matchLocalGoals, $matchVisitorGoals, $poolLocalGoals, $poolVisitorGoals){
		return ($matchLocalGoals == $poolLocalGoals && $matchVisitorGoals == $poolVisitorGoals) ? true : false;
	}

	private function getPonderationWinner($game){
		$ponderation = 1;
		
		if($game < Game::ROUND_OF_16){
			return 1;
		}elseif($game >= Game::	ROUND_OF_16 && $game < Game::QUARTER_FINALS){
			return 2;
		}elseif($game >= Game::	QUARTER_FINALS && $game < Game::SEMI_FINALS){
			return 4;
		}elseif($game >= Game::	SEMI_FINALS && $game < Game::THIRD_PLACE){
			return 6;
		}elseif($game >= Game::	THIRD_PLACE && $game < Game::THE_FINAL){
			return 10;
		}

		return $ponderation;
	}

	private function getPonderationScore($game){
		$ponderation = 1;
		
		if($game < Game::ROUND_OF_16){
			return 1;
		}elseif($game >= Game::	ROUND_OF_16 && $game < Game::QUARTER_FINALS){
			return 2;
		}elseif($game >= Game::	QUARTER_FINALS && $game < Game::SEMI_FINALS){
			return 4;
		}elseif($game >= Game::	SEMI_FINALS && $game < Game::THIRD_PLACE){
			return 6;
		}elseif($game >= Game::	THIRD_PLACE && $game < Game::THE_FINAL){
			return 12;
		}

		return $ponderation;
	}

	// public function calculatePointsAll(){
	// 	$points = 0;

	// 	//Get only games that have been played
	// 	$games = Game::where('played', 1)->get();

	// 	//
	// 	foreach ($games as $game) {
	// 		echo '***' . $game->id . ' -> ';
	// 		echo $game->country1_goals;
	// 		echo ' - ';
	// 		echo $game->country2_goals;
	// 		echo '<br>';
	// 		$scoreCards = Score::where('game_id', $game->id)->get();
			
	// 		//Get match result: Local won(1), Visitor won(2) or draw(3)
	// 		$matchResult = self::getMatchResult($game->country1_goals, $game->country2_goals);

	// 		foreach ($scoreCards as $score) {
	// 			$poolResult = self::getMatchResult($score->country1_goals, $score->country2_goals);

	// 			//Check if the pool result is the same as the match
	// 			if($matchResult == $poolResult){
	// 				$points++; //To do - This only works for 1st phase only

	// 				if(self::isScoreExact($game->country1_goals, $game->country2_goals, $score->country1_goals, $score->country2_goals)){
	// 					$points++;
	// 				}

	// 				$score->points = $points;
	// 				$score->save();
	// 			}

	// 			echo $score->id . ' -> ';
	// 			echo $score->country1_goals;
	// 			echo ' - ';
	// 			echo $score->country2_goals;
	// 			echo ' Pts: ' . $points;
	// 			echo '<br>';

	// 			//Reset points
	// 			$points = 0;
	// 		}
	// 	}
	// }

}