<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of customers
	 *
	 * @return Response
	 */
	public function index()
	{
		$lang = Session::get('lang');
		if (Input::get('sort') && Input::get('order')) 
		{
			$sort = Input::get('sort');
			$order = Input::get('order');
			$ascOrDesc = $order;
		} else {
			$sort = 'name';
			$order = 'asc';
			$ascOrDesc = $order;
		}

		if(Auth::user()->role == 3){
			
			$usersInGroup = array();

			$adminInfo = UserGroup::where('user_id', '=', Auth::user()->id)->first();
			$groupUsers = UserGroup::where('group_id', '=', $adminInfo->group_id)->get();

			$users = User::select(DB::raw('users.*'))
                ->leftJoin('user_groups', 'user_groups.user_id', '=', 'users.id')
                ->where('users.id', '<>', 1)
                ->where('user_groups.group_id', '=', $adminInfo->group_id)
                ->orderBy('users.id', 'ASC')
                // ->take(15)
                ->get();

                //print_r($users);
		}
		else if( Auth::user()->id ==1){
			$adminInfo = '';
			$users = User::exceptSuperuser()->orderBy('id')->get();
			$userInfo = '';
		}
		else{
			return Redirect::action('HomeController@showDashboard');
		}

		return View::make('home.users')->with(array('users'=> $users, 'ascOrDesc'=> $ascOrDesc, 'adminInfo'=>$adminInfo, 'lang'=>$lang ));
	} 

	/**
	 * Show the form for creating a new customer
	 *
	 * @return Response
	 */
	public function create()
	{
		$lang = Session::get('lang');
		if(Auth::user()->role == 1){
			return Redirect::action('HomeController@showDashboard');
		}
		
		$userInfo = UserGroup::where('user_id', '=', Auth::user()->id )->first();
		$countries = Country::orderBy('name', 'ASC')->get();
		
		if(Auth::user()->role == 3){

		$groups = Group::where('id', '=', $userInfo->group_id)->get();

		}else{
			$groups = Group::orderBy('id', 'DESC')->get();
		}

		$userCountries = Country::where('name', '=', 'United States')->orwhere('name', '=', 'Mexico')->orderBy('name', 'ASC')->get();

		return View::make('home.user-create-edit')->with(array('isEdit'=>false, 'countries'=>$countries, 'userCountries'=>$userCountries, 'groups'=>$groups, 'lang'=>$lang));
	}

	/**
	 * Store a newly created customer in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), User::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$user = new User();
		$user->username = Input::get('username');
		$user->first_name = Input::get('first_name');
		$user->last_name = Input::get('last_name');
		$user->email = Input::get('email');
		$user->password = Input::get('password');
		$user->payment_received = intval(Input::get('payment_received'));
		$user->country = Input::get('country');
		
		if(Auth::user()->role == 3){
			$user->role = 1;
		}else{
			$user->role = Input::get('role');
		}
		
		$user->save();

		$userGroup = new UserGroup();
		$userGroup->user_id = $user->id; 
		$userGroup->group_id = Input::get('group');
		$userGroup->save();


		return Redirect::route('users.index');
	}

	/**
	 * Display the specified customer.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified customer.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$lang = Session::get('lang');
		if(Auth::user()->role == 1 && Auth::user()->id != $id){
			return Redirect::action('HomeController@showDashboard');
		}

		$userInfo = UserGroup::where('user_id', '=', Auth::user()->id )->first();
		$thisUserInfo = UserGroup::where('user_id', '=', $id )->first();

		if(Auth::user()->role == 3){
			if($userInfo->group_id != $thisUserInfo->group_id)
			{
				return Redirect::action('HomeController@showDashboard');
			}
		}

		$user = User::find($id);

		$countries = Country::orderBy('name', 'ASC')->get();
		
		if(Auth::user()->role == 3){

			$groups = Group::where('id', '=', $userInfo->group_id)->get();

		}else{
			$groups = Group::orderBy('id', 'DESC')->get();
		}
		

		$userCountries = Country::where('name', '=', 'United States')->orwhere('name', '=', 'Mexico')->orderBy('name', 'ASC')->get();

		$data = array(
			'user' => $user,
			'isEdit' => true,
			'userCountries' => $userCountries,
			'groups' => $groups,
			'countries' => $countries,
			'lang' => $lang
		);

		return View::make('home.user-create-edit')->with($data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$user = User::findOrFail($id);

		$newRules = User::$rules;
		$newRules['username'] = 'min:3|max:20|alpha_num';
		$newRules['password'] = 'min:8|max:40';
		// $newRules['email'] = 'required|email|unique:customers,email,' . $id;

		$validator = Validator::make($data = Input::all(),  $newRules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		//$user->username = Input::get('username');
		$user->first_name = Input::get('first_name');
		$user->last_name = Input::get('last_name');
		 if (Input::get('password')!=''){
			$user->password = Input::get('password');
		 }
		$user->email = Input::get('email');
		$user->payment_received = intval(Input::get('payment_received'));
		$user->country = Input::get('country');
		$user->role = Input::get('role');
		$user->save();
		
		if($user->id != 1){ 
			$userGroup = UserGroup::where('user_id', '=', $user->id)->first();
			$userGroup->group_id = Input::get('group');
			$userGroup->save();
		}

		return Redirect::route('users.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$thisUser = User::find($id);

		$thisUser->userGroups()->delete();
		// delete the user
		$thisUser->delete();

		return Redirect::route('users.index');
	}


}