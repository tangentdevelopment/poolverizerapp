<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@showIndex');

Route::get('users', function()
{
    return View::make('users');
});

Route::get('/login', 'HomeController@showLogin');
Route::post('/login', 'HomeController@doLogin');
Route::get('/register', 'HomeController@showRegister');
Route::post('/register', 'HomeController@doRegister');

// Authenticated Routes for All Users
//
Route::group(array('before' => 'auth'), function()
{
	Route::get('/logout', 'HomeController@logout');

	Route::get('/dashboard', 'HomeController@showDashboard');
	
	Route::get('/dashboard/{lang}', 'HomeController@showDashboardLang');

	Route::get('/rankings/', 'HomeController@showRankings');
	
	Route::get('/rules', 'HomeController@showRules');

	Route::get('/cats_awarded', 'HomeController@showGameResults');
	Route::get('/cats_awarded_entry', 'HomeController@showGameResultsEntry');
	Route::get('/game_results/octavos', 'HomeController@doRoundOf16');
	Route::get('/game_results/cuartos', 'HomeController@doRoundOf4');
	Route::get('/game_results/semis', 'HomeController@doSemifinals');
	Route::get('/game_results/final', 'HomeController@doFinals');

	Route::resource('users', 'UserController');

	Route::get('/user_picks/{id}', 'HomeController@showUserScores');

	Route::get('/user_scores/{id}/edit', 'HomeController@editUserScores');
	Route::post('/user_scores/{id}/edit', 'HomeController@doUserScores');

	Route::post('/save_user_picks', 'HomeController@saveUserPicks');

	Route::get('game_results/edit', 'HomeController@editGameResults');
	Route::post('game_results/edit', 'HomeController@doGameResults');
	Route::get('game_results/update_points', 'HomeController@calculatePointsAll');

	Route::get('cat_award/{nid}', 'HomeController@enterCatWinner');
});